<?php

namespace Modules\Contract\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Config;
use App\Models\BlockChainModel;
use Illuminate\Support\Facades\Crypt;

class ContractController extends Controller
{
    private $paginate = 0;

    function __construct()
    {
        $this->paginate = Config::get('app.pagesNumber');
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {

        if( request()->ajax() ) {

            $contracts = BlockChainModel::where('index','!=',0)
            ->orderBy('id', 'desc')
            ->paginate($this->paginate);
            //obtenemos la lista de los bloques validados
            $verifityBlock = $this->_verifyBlock($contracts);
            $list = ['contracts' => $contracts, 'verifityBlock' =>$verifityBlock];
            //retornamos el resultado como json
            return response()->json($list);

        }else{

            //dump($verifityBlock);
            return  view('contract::index');
            
        }

    }


    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        if( request()->ajax() ) {

            $contract = BlockChainModel::findOrFail($id);
            //retornamos el resultado como json
            return response()->json($contract);

        }else{

            //obtenemos el contrato por id
            $contract = BlockChainModel::findOrFail($id);
            //obtenemos, desencriptamos y convertimos en vector los datos 
            //de browser, device e IP
            $nonce = explode('_',decrypt($contract->nonce));
            return view('contract::show',compact('contract','id','nonce'));
            
        }
        
    }
    /**
     * Privada que verifica si el bloque es correcto y lo monta para 
     * presentarlo en un vector, retornando el resultado
     */
    private function _verifyBlock($blocks){

        $verifyBlocks = [];
        foreach ($blocks as $key => $block) {
            
            $verifyBlocks[$block->id] = $block->hash == $this->_getHash($block);
            //$verifyBlocks[$block->hash] = $this->_getHash($block);
        }
        return $verifyBlocks;
    }

    private function _getHash($block) {

        $otherParam = $block->file_number.$block->document_number.$block->code.$block->image.$block->signature;
        $b = $block->index.$block->previous_hash.$block->timestamp.$block->data.$otherParam.$block->nonce;
        //$b = $block->index.$block->previous_hash.$block->timestamp.((string)$block->data);
        return hash("sha256", $b);
    }

}
