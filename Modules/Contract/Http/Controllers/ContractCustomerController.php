<?php

namespace Modules\Contract\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Auth;
use App\Models\BlockChainModel;
use App\User;

class ContractCustomerController extends Controller
{
    private $paginate = 0;

    function __construct()
    {
        $this->paginate = Config::get('app.pagesNumber');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        if( request()->ajax() ) {
            //obtenemos el listado de contratos del usuario
            $documentNumber = User::with('user_data')->find(Auth::id());
            $contracts = BlockChainModel::where('document_number',$documentNumber->user_data->document_number)
            ->orderBy('id', 'desc')
            ->paginate($this->paginate);
            //obtenemos la lista de los bloques validados
            //$verifityBlock = $this->_verifyBlock($contracts);
            //$list = ['contracts' => $contracts, 'verifityBlock' =>$verifityBlock];
            //retornamos el resultado como json
            return response()->json($contracts);

        }else{

            return  view('contract::index');
        }
        
    }
}