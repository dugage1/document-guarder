const SITE_URL = document.head.querySelector('meta[name="site-url"]').content;
const MODULE_URL = document.head.querySelector('meta[name="module-url"]').content;
const CONTRACT_ID = document.querySelector('#contract_id').value;
//compnente àra editor de texto
import {VueEditor} from "vue2-editor";

if (document.querySelector('#show_contract_component')) {

    var show_contract_component = new Vue({

        el: '#show_contract_component',
        components: {VueEditor},
        data:{

            errorCode: null,
            isLoading: false,
            contractText: null,
        },
        methods: {

            _loadData() {

                //montamos la url para la consulta
                let url = SITE_URL + '/' + MODULE_URL + '/' + 'show/' + CONTRACT_ID
                //realizamos la consutla mediante ajax
                axios.get(url).then((response) => {

                    this.contractText = response.data.data_decrypt; 

                }).catch(error => {

                    this.errorCode = error.response;
                });
            }

        },
        mounted() {

            this._loadData();
        },

    });
}
