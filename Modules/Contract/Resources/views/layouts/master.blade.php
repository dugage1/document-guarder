@extends('layouts.app')

@section('meta') 

    <meta name="module-url" content="{{ Request::segment(1) }}">
    <meta name="method" content="{{ Request::segment(2) != '' ? Request::segment(2) : 'index' }}">

@stop

@section('js')

    @if( Request::segment(2) == '' OR Request::segment(2) == 'customer')

        <script src="{{asset('/js/table_data.js')}}"></script>

    @elseif( Request::segment(2) == 'show' )

        <script src="{{asset('/js/show_contract.js')}}"></script>
        <style>
            .ql-toolbar{display:none;}
            #show_contract_component{border-top:1px solid #ccc;}
        </style>

    @endif

@stop