@extends('contract::layouts.master')

@section('subheader_title')
    <i class="flaticon-file-2"></i> {{ trans('app.contracts.contracts') }}
@stop

@section('subheader_desc')
    {{ trans('app.show') }} {{ trans('app.contracts.contract') }} {{ $contract->file_number }}
@stop

@section('content')

    <div class="kt-section col-md-12">

        @if (Auth::User()->isA('customer'))
            @include('document::partials.menubars.menu_bar_customer')
        @endif

        @if (Auth::User()->isA('admin'))
            @include('document::partials.menubars.menu_bar_admin')
        @endif

    </div>

    <div class="col-md-12">
    
        <div id="crete_or_update__componet" class="kt-portlet">
            
            <div class="kt-portlet__body">

                @if (Auth::User()->isA('admin'))
                
                    <div class="form-group col-4">
                        <label>{{ trans('app.title') }}</label>
                        <input type="text" disabled class="form-control" value="{{$contract->title}}">
                    </div>

                    <div class="form-group col-4">
                        <label>{{ trans('app.date') }}</label>
                        <input type="text" disabled class="form-control" value="{{$contract->created_at}}">
                    </div>

                    <div class="form-group col-6">
                        <label>Previous hash</label>
                        <input type="text" disabled class="form-control" value="{{$contract->previous_hash}}">
                    </div>

                    <div class="form-group col-6">
                        <label>Hash</label>
                        <input type="text" disabled class="form-control" value="{{$contract->hash}}">
                    </div>

                    <div class="form-group col-6">
                        <label>Datos de localización</label>
                        <input type="text" disabled class="form-control" value="IP:{{$nonce[0]}}, País:{{$nonce[1]}}, Zona:{{$nonce[2]}}, Ciudad:{{$nonce[3]}}, Latitud:{{$nonce[4]}}, Longitud:{{$nonce[5]}}">
                    </div>

                    <div class="form-group col-6">
                        <label>Datos de dispositivo</label>
                        <input type="text" disabled class="form-control" value="Dispositivo:{{$nonce[6]}}, Modelo:{{$nonce[7]}}, SO:{{$nonce[8]}}, Navegador:{{$nonce[9]}}">
                    </div>

                @endif

                <div id="show_contract_component" class="form-group">
                
                    <input type="hidden" name="contract_id" id="contract_id" value="{{ $id }}">
                    <vue-editor disabled v-model="contractText"></vue-editor>

                </div>

            </div>

        </div>

    </div>

@stop