<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['web', 'auth'], 'prefix' => 'contract'], function() {
    Route::get('/', 'ContractController@index')->name('contract');
    Route::get('/show/{id}', 'ContractController@show')->name('contract.show')->where('id', '[0-9]+');
    //rutas para ContractCustomerController
    Route::get('/customer', 'ContractCustomerController@index')->name('contract.customer');
});
