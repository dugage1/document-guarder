<?php

namespace Modules\Customer\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Config;
use Modules\Customer\Http\Requests\CustomerStoreRequest;
use Modules\Customer\Http\Requests\CustomerUpdateRequest;
use Bouncer;
use App\User;
use App\Models\UserData;
use App\Models\Way;
use App\Models\Province;

class CustomerController extends Controller
{
    private $paginate = 0;

    function __construct()
    {
        $this->paginate = Config::get('app.pagesNumber');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        if( request()->ajax() ) {

            $customers = User::with('user_data')->whereIs('customer')->paginate($this->paginate);
            return response()->json($customers);

        }else{

          return view('customer::index');
        }

    }

    /**
     * get all customers
     * @return Response
     */
    public function getAllCustomers(){

        if( request()->ajax() ) {
           
            $customers = UserData::select('document_number')
            ->get();
            return response()->json($customers);

        }else{

            abort(404);
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        //todos los tipos de vía
        $ways = Way::orderBy('name')->get();
        $provinces = Province::orderBy('name')->get();
        return view('customer::create',compact('ways','provinces'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(CustomerStoreRequest $request)
    {
        if( request()->ajax() ) {
            //instanciamos User
            $user = new User;
            //seteamos los datos
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            //guardamos
            $user->save();
            //instanciamos UserData
            $userData = new UserData;
            //seteamos los datos
            $userData->user_id = $user->id;
            $userData->name = $request->name;
            $userData->first_name = $request->first_name;
            $userData->last_name = $request->last_name;
            $userData->company_name = $request->company_name;
            $userData->document_number = $request->document_number;
            $userData->telephone = $request->telephone;
            $userData->way_type = $request->way_type;
            $userData->address = $request->address;
            $userData->city = $request->city;
            $userData->zip = $request->zip;
            $userData->province = $request->province;
            $userData->contract_detail = $request->contract_detail;
            $userData->service_specification = $request->service_specification;
            //guardamos
            $userData->save();
            //asignamos el rol
            Bouncer::assign('customer')->to($user);
            //retornamos el usuario
            return response()->json($user);

        }else{

            abort(404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        if( request()->ajax() ) {

            $customer = User::with('user_data')
            ->whereIs('customer')
            ->find($id);
            //si customer es vacío, le pasamos un array con los datos vacíos
            //es necesario para que vue tenga el vector con sus parametros en create
            empty($customer) ? $customer = $this->_getDefaultResult() : '';
            //retornamos los datos
            return response()->json($customer);


        }else{

            //todos los tipos de vía
            $ways = Way::orderBy('name')->get();
            $provinces = Province::orderBy('name')->get();
            return view('customer::edit',compact('ways','id','provinces'));
        }

    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update($id, CustomerUpdateRequest $request)
    {
        if( request()->ajax() ) {
            //obtenemos el usuario y sus datos para editarlo
            $user = User::find($id);
            $userData = UserData::where('user_id',$id)->first();
            //seteamos los datos
            $user->name = $request->name;
            $user->email = $request->email;
            !empty( $request->password ) ? $user->password = bcrypt($request->password) : '';
            //datos usuario
            $userData = UserData::where('user_id',$id)->first();
            $userData->name = $request->name;
            $userData->first_name = $request->first_name;
            $userData->last_name = $request->last_name;
            $userData->company_name = $request->company_name;
            $userData->document_number = $request->document_number;
            $userData->telephone = $request->telephone;
            $userData->way_type = $request->way_type;
            $userData->address = $request->address;
            $userData->city = $request->city;
            $userData->zip = $request->zip;
            $userData->province = $request->province;
            $userData->contract_detail = $request->contract_detail;
            $userData->service_specification = $request->service_specification;
            //guardamos los datos
            $user->save();
            $userData->save();

        }else{

            abort(404);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        if( request()->ajax() ) {

            User::destroy($id);

        }else{

            abort(404);
        }
    }
    /**
     * Método encargado de realizar las consultas en forma de búsqueda y retornar el resultado 
     * @param query -- parametro encargado de almacenar los datos pasados por el campo
     * 
     */
    public function search($query)
    {
        if( request()->ajax() ) {

            $customers = User::with('user_data')->whereIs('customer')
            ->whereHas('user_data', function ($q) use ($query) {
                $q->where('document_number', 'LIKE', '%'.$query.'%');
            })
            ->orWhere('name','LIKE','%' . $query . '%')
            ->paginate($this->paginate);

            return response()->json($customers);

        }else{

            abort(404);
        }
    }
    /**
     * get default object if empty
     * @return Response
     */
    private function _getDefaultResult($test = false) 
    {
        if( $test ) {

            $obj = [

                'name' => 'name',
                'email' => 'isporer22@example.com',
                'user_data' => [
                    'address' => '96  Settlers Lane',
                    'city'  => 'Dos Hermanas',
                    'document_number'  => '08910658K',
                    'name' => 'name',
                    'first_name'  => 'First',
                    'last_name'  => 'Last',
                    'telephone'  => '123456789',
                    'way_type'  => 'CL',
                    'zip'  => '41700',
                    'province' => 'Sevilla',
                    'contract_detail' => 'detail',
                    'service_specification' => 'specification',
                    'company_name' => 'Nombre empresa',
                ]
            ];

        }else{

            $obj = [

                'name' => '',
                'email' => '',
                'user_data' => [
                    'address' => '',
                    'city'  => '',
                    'document_number'  => '',
                    'first_name'  => '',
                    'name' => '',
                    'last_name'  => '',
                    'telephone'  => '',
                    'way_type'  => '',
                    'zip'  => '',
                    'province' => '',
                    'contract_detail' => '',
                    'service_specification' => '',
                    'company_name' => '',
                ]
            ];
        }
        
        return (object)$obj;
    }
}
