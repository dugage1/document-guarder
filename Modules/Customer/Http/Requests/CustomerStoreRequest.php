<?php

namespace Modules\Customer\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class CustomerStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'name' => 'required',
            'email' => 'required|unique:users,email',
            'password' => 'required|min:8',
            'first_name' => 'required',
            'last_name' => '',
            'way_type' => 'required',
            'address' => 'required',
            'city' => 'required',
            'zip' => 'required',
            'document_number' => 'required|unique:user_datas,document_number',
            'telephone' => 'required|min:9',
            'province' => 'required',
            'company_name' => 'required',
            'contract_detail' => '',
            'service_specification' => '',
            
        ];
    }

}