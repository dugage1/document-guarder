<?php

namespace Modules\Customer\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class CustomerUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->id;

        return [

            'name' => 'required',
            'email' => 'required|unique:users,email,'.$id.',id',
            'password' => '',
            'first_name' => 'required',
            'last_name' => '',
            'way_type' => 'required',
            'address' => 'required',
            'city' => 'required',
            'zip' => 'required',
            'document_number' => 'required|unique:user_datas,document_number,'.$id.',user_id',
            'telephone' => 'required|min:9',
            'province' => 'required',
            'company_name' => 'required',
            'contract_detail' => '',
            'service_specification' => '',
            
        ];
    }

}