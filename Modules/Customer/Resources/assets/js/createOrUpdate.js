/**PAQUETES UTILIZADOS*/

import VeeValidate from 'vee-validate';
//cargamos idioma español para las validaciones
const VueValidationEs = require('vee-validate/dist/locale/es');
//y le pasamos la constante a la configuración
const config = {
    locale: 'es',
    events: 'blur',
    dictionary: {
        es: VueValidationEs

    }
};
//instanciamos vee validate
Vue.use(VeeValidate,config);
// Vue mask, para las mascaras de los campos
const VueInputMask = require('vue-inputmask').default;
Vue.use(VueInputMask);
//paqueta para la barra de carga de datos
import Vue from 'vue';
import zloading from 'z-loading';
import 'z-loading/dist/z-loading.css';
Vue.use(zloading);

//compnente àra editor de texto
import {VueEditor} from "vue2-editor";

/***************************************************/

// Regla de validación que comprueba el NIF/NIE válido
VeeValidate.Validator.extend('documentNumber', {
    getMessage: field => 'El ' + field + ' no es válido',
    validate: value => {
        var validChars = 'TRWAGMYFPDXBNJZSQVHLCKET';
        var nifRexp = /^[0-9]{8}[TRWAGMYFPDXBNJZSQVHLCKET]{1}$/i;
        var nieRexp = /^[XYZ]{1}[0-9]{7}[TRWAGMYFPDXBNJZSQVHLCKET]{1}$/i;
        var cifRexp = /^([ABCDEFGHJKLMNPQRSUVW])(\d{7})([0-9A-J])$/;
        var str = value.toString().toUpperCase();

        if (!nifRexp.test(str) && !nieRexp.test(str) && !cifRexp.test(str)) return false;

        var nie = str
            .replace(/^[X]/, '0')
            .replace(/^[Y]/, '1')
            .replace(/^[Z]/, '2');

        var letter = str.substr(-1);
        var charIndex = parseInt(nie.substr(0, 8)) % 23;

        if (validChars.charAt(charIndex) === letter) {
            return true;
        }else{
            return validCIF(value);
            //return false;
        }

    }
});

function validCIF( cif ) {

    var CIF_REGEX = /^([ABCDEFGHJKLMNPQRSUVW])(\d{7})([0-9A-J])$/;
    var match = cif.match( CIF_REGEX );
    var letter  = match[1],
        number  = match[2],
        control = match[3];

    var even_sum = 0;
    var odd_sum = 0;
    var n;

    for ( var i = 0; i < number.length; i++) {
      n = parseInt( number[i], 10 );

      // Odd positions (Even index equals to odd position. i=0 equals first position)
      if ( i % 2 === 0 ) {
        // Odd positions are multiplied first.
        n *= 2;

        // If the multiplication is bigger than 10 we need to adjust
        odd_sum += n < 10 ? n : n - 9;

      // Even positions
      // Just sum them
      } else {
        even_sum += n;
      }

    }

    var control_digit = (10 - (even_sum + odd_sum).toString().substr(-1) );
    var control_letter = 'JABCDEFGHI'.substr( control_digit, 1 );

    // Control must be a digit
    if ( letter.match( /[ABEH]/ ) ) {
      return control == control_digit;

    // Control must be a letter
    } else if ( letter.match( /[KPQS]/ ) ) {
      return control == control_letter;

    // Can be either
    } else {
      return control == control_digit || control == control_letter;
    }

};

const SITE_URL = document.head.querySelector('meta[name="site-url"]').content;
const MODULE_URL = document.head.querySelector('meta[name="module-url"]').content;
//const METHOD = document.head.querySelector('meta[name="method"]').content;
const CUSTOMER_ID = document.querySelector('#customer_id').value;

if (document.querySelector('#crete_or_update__componet')) {

    var crete_or_update__componet = new Vue({

        el: '#crete_or_update__componet',
        components: {VueEditor},
        data:{

            formFields: [],
            errorCode: null,
            erroValidate: false,
            password: null,
            isLoading: false,
            cities: [],
            saved: false,
            routes: [],

        },
        methods: {
            //método para crear o actualizar los datos
            saveData : function() {
                //sobreescribimos errorCode a null
                this.errorCode = null;
                //url de la consutla
                let url = SITE_URL + '/' +  MODULE_URL;
                CUSTOMER_ID > 0 ? url += '/update/' + CUSTOMER_ID : url += '/store';
                //capturamos todos los campos de formulario
                const formData = new FormData(this.$refs['formMain']);
                const data = {};

                for (let [key, val] of formData.entries()) {
                    Object.assign(data, { [key]: val })
                }
                //almacenamos los datos de los editores
                Object.assign(data, { 'contract_detail': this.formFields.user_data.contract_detail });
                Object.assign(data, { 'service_specification': this.formFields.user_data.service_specification });
                //console.log(data);
                this.$validator.validateAll().then(() => {
                       
                    if ( !this.errors.any() ) {
                        
                        axios.post(url,data).then((response) => {

                            console.log(response);
                            this.getLoading();
                            //saved = true para mostrar la botonera alternativa
                            this.saved = true;
                            //montamos las rutas necesarias
                            this.routes = {
                                index: SITE_URL + '/' +  MODULE_URL, 
                                create: SITE_URL + '/' +  MODULE_URL + '/create',
                                edit: SITE_URL + '/' +  MODULE_URL + '/edit/' + response.data.id,
                            };

                        }).catch(error => {

                            this.errorCode = error.response;
                        });

                    }else{

                        this.erroValidate = true;
                    }

                });

            },
            //lanza la barra de carga
            getLoading() {

                this.isLoading = true;
                this.$zLoading.open();

                setTimeout(() => {

                  this.$zLoading.close();
                  this.isLoading = false;

                }, 3000);

            },

            /**
             * 
             */
            getCities(loadData) {
    
                const data = {'province': this.formFields.user_data.province};
                let url = SITE_URL + '/cities_by_province';
                axios.post(url,data).then((response) => {
                    console.log(response.data);
                    if( response.data )
                        this.cities = response.data[0];
                    
                    if( !loadData ) {
                        this.formFields.user_data.zip = response.data[1].code;
                        this.formFields.user_data.city = '';
                    }
    
                }).catch(error => {
                
                    this.errorCode = error.response;

                });
            },

            //consutla y carga los parametros del formulario, estos pueden ser vacios
            //para create o con datos en caso de edit
            _loadData() {
                //montamos la url para la consulta
                let url = SITE_URL + '/' + MODULE_URL + '/' + 'edit/' + CUSTOMER_ID
                //realizamos la consutla mediante ajax
                axios.get(url).then((response) => {

                    this.formFields = response.data;
                    this.getCities(true);

                }).catch(error => {

                    this.errorCode = error.response;
                });

            }
        },
        mounted() {

            this._loadData();
        },
        
    });
}
