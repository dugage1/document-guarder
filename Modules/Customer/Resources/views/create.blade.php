@extends('customer::layouts.master')

@section('subheader_title')
    <i class="flaticon-customer"></i> {{ trans('app.customers.customers') }}
@stop

@section('subheader_desc')
    {{ trans('app.create') }} {{ trans('app.customers.newCustomer') }} 
@stop

@section('content')
    
    <div class="col-md-12">

        <div id="crete_or_update__componet" class="kt-portlet">
            
            @include('customer::partials.forms.main_form')

        </div>

    </div>

@stop
