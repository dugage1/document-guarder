@extends('customer::layouts.master')

@section('subheader_title')
    <i class="flaticon-customer"></i> {{ trans('app.customers.customers') }}
@stop

@section('subheader_desc')
    {{ trans('app.listOf') }} {{ trans('app.customers.customers') }}
@stop

@section('content')

    <div id="table-data" class="kt-portlet kt-portlet--mobile">
        
        <div class="kt-portlet__head kt-portlet__head--lg">


            <div class="kt-portlet__head-toolbar col-xl-4 p-0">
                
                <a href="{{ route('customer.create') }}" class="btn btn-brand btn-icon-sm" aria-haspopup="true" aria-expanded="false">
                    <i class="flaticon-add-circular-button"></i>{{ trans('app.create') }} {{ trans('app.customers.newCustomer') }}  	
                </a>

            </div>

            <div class="kt-portlet__head-toolbar col-xl-3 p-0">
                
                <div class="kt-input-icon kt-input-icon--left">
                    <input v-model="searchParam" @keyup="getDataBySearchParam" name="searchParam" type="text" class="form-control" placeholder="{{ trans('app.search') }}..." id="generalSearch">
                    <span class="kt-input-icon__icon kt-input-icon__icon--left">
                        <span><i class="flaticon-search"></i></span>
                    </span>
                </div>
            
            </div>

        </div>

        <div class="kt-portlet__body">

            <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--loaded" id="local_data">
                
                @include('customer::partials.tables.index_table')
            
            </div>

        </div>

    </div>
    
@stop