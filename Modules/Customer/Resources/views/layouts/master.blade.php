@extends('layouts.app')

@section('meta') 

    <meta name="module-url" content="{{ Request::segment(1) }}">
    <meta name="method" content="{{ Request::segment(2) != '' ? Request::segment(2) : 'index' }}">

@stop

@section('js')

    @if( Request::segment(2) == 'create' OR Request::segment(2) == 'edit' )
        
        <script src="{{asset('/js/customer_create_or_update.js')}}"></script>

    @else( Request::segment(2) == '' )

        <script src="{{asset('/js/table_data.js')}}"></script>

    @endif

@stop