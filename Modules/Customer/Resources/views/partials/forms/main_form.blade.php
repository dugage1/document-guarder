<form ref="formMain" class="kt-form">

    @csrf
    <input type="hidden" name="customer_id" id="customer_id" value="{{ $id ?? 0 }}">

    <div class="kt-portlet__body">

        <div class="form-group" :class="{'has-error': errors.has('name') }">
            <label>{{ trans('app.name') }}</label>
            <input type="text" class="form-control" name="name" id="name" data-vv-as="{{ trans('app.name') }}" v-validate="'required|alpha_spaces'" v-model="formFields.user_data.name">
            <span class="alert-danger" v-text="errors.first('name')"></span>
        </div>

        <div class="form-group" :class="{'has-error': errors.has('first_name') }">
            <label>{{ trans('app.firstName') }}</label>
            <input type="text" class="form-control" name="first_name" id="first_name" data-vv-as="{{ trans('app.firstName') }}" v-validate="'required|alpha_spaces'" v-model="formFields.user_data.first_name">
            <span class="alert-danger" v-text="errors.first('first_name')"></span>
        </div>

        <div class="form-group" :class="{'has-error': errors.has('last_name') }">
            <label>{{ trans('app.lastName') }}</label>
            <input type="text" class="form-control" name="last_name" id="last_name" data-vv-as="{{ trans('app.lastName') }}" v-validate="'alpha_spaces'" v-model="formFields.user_data.last_name">
            <span class="alert-danger" v-text="errors.first('last_name')"></span>
        </div>

        <div class="form-group" :class="{'has-error': errors.has('company_name') }">
            <label>{{ trans('app.companyName') }}</label>
            <input type="text" class="form-control" name="company_name" id="company_name" data-vv-as="{{ trans('app.companyName') }}" v-validate="'required'" v-model="formFields.user_data.company_name">
            <span class="alert-danger" v-text="errors.first('company_name')"></span>
        </div>

        <div class="form-group" :class="{'has-error': errors.has('document_number') }">
            <label>{{ trans('app.documentNumber') }}</label>
            <input type="text" class="form-control" name="document_number" id="document_number" data-vv-as="{{ trans('app.documentNumber') }}" v-validate="'required|min:9|max:9'" v-model="formFields.user_data.document_number">
            <span class="alert-danger" v-text="errors.first('document_number')"></span>
        </div>

        <div class="form-group" :class="{'has-error': errors.has('telephone') }">
            <label>{{ trans('app.telephone') }}</label>
            <input type="text" class="form-control" name="telephone" id="telephone" data-vv-as="{{ trans('app.telephone') }}" v-mask="{mask: '999 999 999', autoUnmask: true}" v-validate="'required|numeric|min:9|max:9'" v-model="formFields.user_data.telephone">
            <span class="alert-danger" v-text="errors.first('telephone')"></span>
        </div>

        <div class="form-group" :class="{'has-error': errors.has('way_type') }">
            <label for="way_type">{{ trans('app.wayType') }}</label>
            <select name="way_type" class="form-control" id="way_type" data-vv-as="{{ trans('app.wayType') }}"  v-validate="'required'" v-model="formFields.user_data.way_type">
                <option></option>
                @foreach ($ways as $way)
                    <option value="{{ $way->code }}">{{ $way->name }}</option>
                @endforeach
            </select>
            <span class="alert-danger" v-text="errors.first('way_type')"></span>
        </div>

        <div class="form-group" :class="{'has-error': errors.has('address') }">
            <label>{{ trans('app.address') }}</label>
            <input type="text" class="form-control" name="address" id="address" data-vv-as="{{ trans('app.address') }}" v-validate="'required'" v-model="formFields.user_data.address">
            <span class="alert-danger" v-text="errors.first('address')"></span>
        </div>

        <div class="form-group" :class="{'has-error': errors.has('province') }">
            <label for="province">{{ trans('app.province') }}</label>
            <select @change="getCities(false)" name="province" class="form-control" id="province" data-vv-as="{{ trans('app.province') }}"  v-validate="'required'" v-model="formFields.user_data.province">
                <option></option>
                @foreach ($provinces as $province)
                    <option value="{{ $province->name }}">{{ $province->name }}</option>
                @endforeach
            </select>
            <span class="alert-danger" v-text="errors.first('province')"></span>
        </div>

        <div class="form-group" :class="{'has-error': errors.has('city') }">
            <label>{{ trans('app.city') }}</label>
            <select :disabled="cities.length == 0 ? '' : disabled" name="city" class="form-control" v-model="formFields.user_data.city" data-vv-as="{{ trans('app.city') }}" v-validate="'required'">
            <option value=""></option>
            <option v-for="city in cities" v-text="city.name" :value="city.name"></option>
        </select>

            <span class="alert-danger" v-text="errors.first('city')"></span>
        </div>

        <div class="form-group" :class="{'has-error': errors.has('zip') }">
            <label>{{ trans('app.zip') }}</label>
            <input type="text" class="form-control" name="zip" id="zip" data-vv-as="{{ trans('app.zip') }}" v-mask="{mask: '99999', autoUnmask: true}" v-validate="'required|numeric|min:5|max:5'" v-model="formFields.user_data.zip">
            <span class="alert-danger" v-text="errors.first('zip')"></span>
        </div>

        <div class="form-group" :class="{'has-error': errors.has('email') }">
            <label>Email</label>
            <input type="text" class="form-control" name="email" id="email" v-validate="'required|email'" v-model="formFields.email">
            <span class="alert-danger" v-text="errors.first('email')"></span>
        </div>

        <div class="form-group" :class="{'has-error': errors.has('password') }">
            <label>{{ trans('app.password') }}</label>
            <input type="password" class="form-control" name="password" id="password" data-vv-as="{{ trans('app.password') }}" @if(isset($id)) v-validate="'min:8'" @else v-validate="'required|min:8'" @endif v-model="password">
            <span class="alert-danger" v-text="errors.first('password')"></span>
        </div>

        <div class="form-group">
            <label> {{ trans('app.customers.contractDetail') }}</label>
            <vue-editor v-model="formFields.user_data.contract_detail"></vue-editor>
        </div>

        <div class="form-group">
            <label> {{ trans('app.customers.servieSpecification') }}</label>
            <vue-editor v-model="formFields.user_data.service_specification"></vue-editor>
        </div>

    </div>

    <div class="kt-portlet__foot">

        @include('layouts.partials.forms.buttons.actions')

    </div>

</form>