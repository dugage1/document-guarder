<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['web', 'auth'], 'prefix' => 'customer'], function() {
    Route::get('/', 'CustomerController@index')->name('customer');
    Route::get('/get-all-customers', 'CustomerController@getAllCustomers');
    Route::get('/create', 'CustomerController@create')->name('customer.create');
    Route::post('/store', 'CustomerController@store');
    Route::get('/edit/{id}', 'CustomerController@edit')->name('customer.edit')->where('id', '[0-9]+');
    Route::post('/update/{id}', 'CustomerController@update')->where('id', '[0-9]+');
    Route::get('/search/{query}', 'CustomerController@search');
    Route::delete('/destroy/{id}', 'CustomerController@destroy')->name('customr.destroy')->where('id', '[0-9]+');
});

