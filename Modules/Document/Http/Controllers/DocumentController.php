<?php

namespace Modules\Document\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Config;
use Modules\Document\Http\Requests\DocumentStoreRequest;
use Modules\Document\Http\Requests\DocumentUpdateRequest;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendSignatureDocument;
use App\Models\Document;
use App\Models\UserData;

class DocumentController extends Controller
{
    private $paginate = 0;

    function __construct()
    {
        $this->paginate = Config::get('app.pagesNumber');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        if( request()->ajax() ) {

            $documents = Document::orderBy('id', 'desc')
            ->paginate($this->paginate);
            //retornamos el resultado como json
            return response()->json($documents);

        }else{

            return  view('document::index');
            
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('document::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(DocumentStoreRequest $request)
    {
        if( request()->ajax() ) {
            //generamos una firma alfanumérica de 6 caracteres
            $code = str_random(6);
            //instanciamos Document
            $document = new Document;
            /**
             * Al setear los datos, el modelo se encarga de 
             * encryptar los datos necesario, y en los métodos de 
             * encryptación adecuados.
             */
            $document->document_number = $request->document_number;
            $document->title = $request->title;
            $document->text_document = $request->text_document;
            $document->code = $code;
            //guardamos
            $document->save();
            //creamos el número de documento
            Document::setFileNumber($document->id);
            //obtenemos los datos del cliente
            $userData = UserData::with('user')
            ->where('document_number',$document->document_number)
            ->first();
            //probando el envío de email
            Mail::to($userData->user->email)->send(new SendSignatureDocument($document,$userData,$code));
            //retornamos los datos
            return response()->json($document);
        }else{

            abort(404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        if( request()->ajax() ) {

            $document = Document::find($id);
            //si document es vacío, le pasamos un array con los datos vacíos
            //es necesario para que vue tenga el vector con sus parametros en create
            empty($document) ? $document = $this->_getDefaultResult() : '';
            //realizamos decryp a los datos necesarios y montamos el objeto
            $obj = [
                'document_number' => $document->document_number,
                'title' => $document->title,
                'text_document' => decrypt($document->text_document)
            ];
            //retornamos los datos
            return response()->json((object)$obj);

        }else{

            return view('document::edit',compact('id'));
        }
        
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(DocumentUpdateRequest $request, $id)
    {
        if( request()->ajax() ) {

            $document = Document::find($id);
            //seteamos los datos
            $document->document_number = $request->document_number;
            $document->title = $request->title;
            $document->text_document = $request->text_document;
            //guardamos
            $document->save();

        }else{

            abort(404);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        if( request()->ajax() ) {

            Document::destroy($id);

        }else{

            abort(404);
        }
    }

    /**
     * Método encargado de realizar las consultas en forma de búsqueda y retornar el resultado 
     * @param query -- parametro encargado de almacenar los datos pasados por el campo
     * 
     */
    public function search($query)
    {
        if( request()->ajax() ) {

            $documents = Document::where('document_number', 'LIKE', '%'.$query.'%')
            ->orWhere('title','LIKE','%' . $query . '%')
            ->orWhere('file_number','LIKE','%' . $query . '%')
            ->paginate($this->paginate);

            return response()->json($documents);

        }else{

            abort(404);
        }
    }

     /**
     * get default object if empty
     * @return Response
     */
    private function _getDefaultResult($test = false) 
    {
        $obj = [

            'document_number' => '',
            'title' => '',
            'text_document' => ''
        ];

        return (object)$obj;
    }
}
