<?php

namespace Modules\Document\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Config;
use App\Http\Controllers\Blockchain\BlockchainController;
use Illuminate\Support\Facades\Crypt;
use App\User;
use App\Models\Document;
use App\Models\BlockChainModel;
use Browser;
use Location;

class DocumentCustomerController extends Controller
{

    private $paginate = 0;
    private $maxBlockChain = 5;

    function __construct()
    {
        $this->paginate = Config::get('app.pagesNumber');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        if( request()->ajax() ) {

            //obtenemos el listado de documentos del cliente
            $documentNumber = User::with('user_data')->find(Auth::id());
            $documents = Document::where('document_number',$documentNumber->user_data->document_number)
            ->orderBy('id', 'desc')
            ->paginate($this->paginate);
            //retornamos el resultado como json
            return response()->json($documents);

        }else{

            return  view('document::index');
        }
    }

    /**
     * Show the specified resource.
     * El método comprueba mediante la privada _havePermission si puedo o 
     * no realizar acciones con el documento
     * @param int $id
     * @return Response
     */
    public function show($id,Request $request)
    {
        //obtenemos el documento por su ID
        $document = Document::findOrFail($id);
        //comprobamos si tenemos permisos
        $permission = $this->_havePermission($document->code,$request->input('code'));
        return view('document::show',compact('document','permission','id'));
    }

    /**
     * 
     */
    public function getDocument($id) {

        if( request()->ajax() ) {

            $document = Document::find($id);
            return response()->json( decrypt($document->text_document) );

        }else{

            abort(404);
        }
    }
    /**
     * Area donde el usuario puede firmar el contrato
     */
    public function signDocument($id,Request $request)
    {
        //obtenemos el documento por su ID
        $document = Document::find($id);
        //comprobamos si tenemos permisos
        $permission = $this->_havePermission($document->code,$request->input('code'));
        return view('document::sign_document',compact('document','permission','id'));
    }
    /**
     * Método donde pasamos de documento a contrato
     * Usando la clase Blockchain, generemos el bloque
     */
    public function setContract($id)
    {
        if( request()->ajax() ) {
            //obtenemos los datos del documento
            $document = Document::find($id);
            //consultamos para obtener el último bloque
            $lastBlock = BlockChainModel::orderBy('id', 'desc')->first();
            //capturamos el resultado de las acciones.
            $result = false;
            /**
             * Comprobamos el index del bloque obtenido, de forma que si
             * este es igual al @param maxBlockChain creamos primero un 
             * bloque Genesis y luego el bloque del documento encadenado a este
             * */ 
            if( $lastBlock->index >= $this->maxBlockChain ) {
                //en este caso, en lugar de @param lasBlock le pasamos los datos obtenidos de setGenesisBlock
                //para vincularlo a la nueva cadena 
                $result = $this->_setBlock($this->_setGenesisBlock(),$document);

            }else{
                
                $result = $this->_setBlock($lastBlock,$document);
            }
            //si la acción se realizo con éxito, borramos el documento de document
            //$result ? Document::destroy($id) : '';
            return response()->json($result);

        }else{

            abort(404);
        }
    }

    /**
     * LandingPage con mensaje al finalizar 
     * correctamente la firma del documento
     */
    public function signedDocument($hash,$id)
    {
        //obtenemos la información del contrato
        $contract = BlockChainModel::findOrFail($id);
        return view('document::signed_document',compact('contract'));
    }

    private function _setGenesisBlock()
    {
        $block = new BlockchainController();
        $newBlock = new BlockChainModel;

        $newBlock->index = $block->index;
        $newBlock->timestamp = $block->timestamp;
        $newBlock->data = $block->data;
        $newBlock->previous_hash = $block->previousHash;
        $newBlock->hash = $block->hash;
        //guardamos el nuevo bloque
        if( $newBlock->save() ) {
            //si se guardo el bloque, pasamos true para dar permisos para continuar la creación del siguiente bloque
            return $newBlock;
        }else{
            //en caso negativo, devolvemos un false e informamos al usuario de lo ocurrido
            return false;

        }
        
    }

    private function _setBlock($lastBlock,$document){

        $index = $lastBlock->index + 1;
        $timestamp = strtotime('now');
        $data = $document->text_document;
        $previousHash = $lastBlock->hash;
        $otherParam = $document->file_number.$document->document_number.$document->code.$document->image.$document->signature;
        $nonce = encrypt($this->_additionalData());
        $block = new BlockchainController($index,$timestamp,$data,$previousHash,$nonce,$otherParam);
        $newBlock = new BlockChainModel;

        $newBlock->index = $block->index;
        $newBlock->nonce = $block->nonce;
        $newBlock->timestamp = $block->timestamp;
        $newBlock->file_number =  $document->file_number;
        $newBlock->title =  $document->title;
        $newBlock->data = $document->text_document;
        $newBlock->document_number =  $document->document_number;
        $newBlock->code = $document->code;
        $newBlock->image = $document->image;
        $newBlock->signature = $document->signature;
        $newBlock->hash = $block->hash;
        $newBlock->previous_hash = $block->previousHash;
        //comprobamos si se guardo el bloque
        if( $newBlock->save() ) {
            //si se guardo el bloque, retornamos el bloque
            return $newBlock;
        }else{
            //en caso negativo, devolvemos un false e informamos al usuario de lo ocurrido
            return false;

        }
    }

    /**
     * Datos adicionales recopilados y almacenados
     * en forma de cadena separada por '_'
     * Los datos serán almacenaos en @param data y retornados
     */
    private function _additionalData()
    {

        $data = '';
        //datos de posición en base IP
        $position = Location::get(\Request::getClientIp());
        //montamos la cadena
        $data .= \Request::getClientIp().'_';
        $data .= $position->countryCode.'_';
        $data .= $position->regionName.'_';
        $data .= $position->cityName.'_';
        $data .= $position->latitude.'_';
        $data .= $position->longitude.'_';
        $data .= Browser::deviceFamily().'_';
        $data .= Browser::deviceModel().'_';
        $data .= Browser::platformName().'_';
        $data .= Browser::browserName();
        //retornamos la cadena
        return $data;
    }
    /**
     * check if user we check if user may watch contract
     * Este método comprueba mediante la firma del documento si puede
     * realizar acciones con este
     * @param permission -- booleano retorna true or false para dar o no permiso al usuario
     */
    private function _havePermission($documentCode,$code = null){

        $permission = false;
        //si signature es distinto de null, entramos y comparamos el valor
        if( $code ) {
            //comprobamos la firma introducida corresponte a este documento
            //si es así, $permission = true
            if (Hash::check($code, $documentCode)) 
                $permission = true;
           
        }
        return $permission;
    }

}