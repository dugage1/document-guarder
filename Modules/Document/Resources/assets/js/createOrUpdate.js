/**PAQUETES UTILIZADOS*/

import VeeValidate from 'vee-validate';
//cargamos idioma español para las validaciones
const VueValidationEs = require('vee-validate/dist/locale/es');
//y le pasamos la constante a la configuración
const config = {
    locale: 'es',
    events: 'blur',
    dictionary: {
        es: VueValidationEs

    }
};
//instanciamos vee validate
Vue.use(VeeValidate,config);


//compnente àra editor de texto
import {VueEditor} from "vue2-editor";

//paqueta para la barra de carga de datos
import Vue from 'vue';
import zloading from 'z-loading';
import 'z-loading/dist/z-loading.css';
Vue.use(zloading);

//Paquete para campos de autocompletado
import Autocomplete from 'vuejs-auto-complete';

/***************************************************/


const SITE_URL = document.head.querySelector('meta[name="site-url"]').content;
const MODULE_URL = document.head.querySelector('meta[name="module-url"]').content;
const DOCUMENT_ID = document.querySelector('#document_id').value;
/**
 * Componente encargado de crear y editar desde el formulario
 * los datos de un documento
 * @param isLoading -- booleano muestra o no el preloader al guardar o editar
 * @param errorCode -- alamcena en caso de error al consultar, los datos de este.
 * @param erroValidate -- booleano, para indicar si tenemos o no error en las validaciones del formulario
 * @param title -- Almacena el título de documento
 * @param text_document -- almacena el texto del documento
 * @param document_number -- almacena el número de documento seleccionado
 * @param customers[] -- colección de números de documentos de clientes para el campo número de docuemto con autocompletado
 */
if (document.querySelector('#crete_or_update__componet')) {

    var crete_or_update__componet = new Vue({

        el: '#crete_or_update__componet',
        components: {VueEditor,Autocomplete},
        data:{

            errorCode: null,
            erroValidate: false,
            isLoading: false,
            title: null,
            text_document: null,
            document_number: null,
            customers: [],
            saved: false,
            routes: [],
        },
        methods: {
            //método para crear o actualizar los datos
            saveData : function() {
                //sobreescribimos errorCode a null
                this.errorCode = null;
                //url de la consutla
                let url = SITE_URL + '/' +  MODULE_URL;
                DOCUMENT_ID > 0 ? url += '/update/' + DOCUMENT_ID : url += '/store';
                //capturamos todos los campos de formulario
                const data = {};
                //creamos el objeto
                Object.assign(data, { 
                    'document_number': this.document_number, 
                    'text_document': this.text_document,
                    'title': this.title,
                })
                //console.log(data);
                this.$validator.validateAll().then(() => {
                       
                    if ( !this.errors.any() ) {
                        
                        axios.post(url,data).then((response) => {

                            //console.log(response);
                            this.getLoading();
                            //saved = true para mostrar la botonera alternativa
                            this.saved = true;
                            //montamos las rutas necesarias
                            this.routes = {
                                index: SITE_URL + '/' +  MODULE_URL, 
                                create: SITE_URL + '/' +  MODULE_URL + '/create',
                                edit: SITE_URL + '/' +  MODULE_URL + '/edit/' + response.data.id,
                            };

                        }).catch(error => {

                            this.errorCode = error.response;
                        });

                    }else{

                        this.erroValidate = true;
                    }

                });

            },
            //lanza la barra de carga
            getLoading() {

                this.isLoading = true;
                this.$zLoading.open();

                setTimeout(() => {

                  this.$zLoading.close();
                  this.isLoading = false;

                }, 3000);

            },
            /**
             * Componente privado que carga la lista de los números de documentos
             * de los clientes para el campo con autocompletado
             */
            _getCustomers(){
                //montamos la url para la consulta
                let url = SITE_URL + '/customer/get-all-customers';
                //realizamos la consutla mediante ajax
                axios.get(url).then((response) => {
                    //console.log(response.data);
                    this.customers = response.data;

                }).catch(error => {

                    this.errorCode = error.response;
                });
                
            },
             /**
              * consutla y carga los parametros del formulario, estos pueden ser vacios
              * para create o con datos en caso de edit
              */
            _loadData() {
                //montamos la url para la consulta
                let url = SITE_URL + '/' + MODULE_URL + '/' + 'edit/' + DOCUMENT_ID
                //realizamos la consutla mediante ajax
                axios.get(url).then((response) => {

                    this.document_number = response.data.document_number; 
                    this.text_document = response.data.text_document;
                    this.title = response.data.title;

                }).catch(error => {

                    this.errorCode = error.response;
                });
                //cargamos la colección de clientes
                this._getCustomers();

            }
        },
        mounted() {

            this._loadData();
        },
        
    });
}
