/**PAQUETES UTILIZADOS*/

import VeeValidate from 'vee-validate';
//cargamos idioma español para las validaciones
const VueValidationEs = require('vee-validate/dist/locale/es');
//y le pasamos la constante a la configuración
const config = {
    locale: 'es',
    events: 'blur',
    dictionary: {
        es: VueValidationEs

    }
};
//instanciamos vee validate
Vue.use(VeeValidate,config);

if (document.querySelector('#permission_show_document')) {
    
    var crete_or_update__componet = new Vue({

        el: '#permission_show_document',
        data:{

            erroValidate: false,
        },
        methods: {

            onSubmit(){
                
                this.$validator.validateAll().then(() => {

                    if ( !this.errors.any() ) {
                        this.$refs.permission_show_document.submit()
                    }else{

                        this.erroValidate = true;
                    }
                });
            }
        }

    });
}