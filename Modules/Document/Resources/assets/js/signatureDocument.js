//paqueta para la barra de carga de datos
import Vue from 'vue';
import zloading from 'z-loading';
import 'z-loading/dist/z-loading.css';
Vue.use(zloading);

/**
 * Vuejs Dialog Plugin | Para los mensajes en las acciones
 */
import VuejsDialog from "vuejs-dialog";
import VuejsDialogMixin from "vuejs-dialog/dist/vuejs-dialog-mixin.min.js";
import 'vuejs-dialog/dist/vuejs-dialog.min.css';
//pasamos la configuración lara VuejsDialog
Vue.use(VuejsDialog, {
    html: true,
    loader: false,
    okText: 'Continuar',
    cancelText: 'Cancelar',
    animation: 'bounce'
});

//compnente àra editor de texto
import {VueEditor} from "vue2-editor";

const DOCUMENT_ID = document.querySelector('#document_id').value;

if (document.querySelector('#sign_document')) {

    const SITE_URL = document.head.querySelector('meta[name="site-url"]').content;
    const MODULE_URL = document.head.querySelector('meta[name="module-url"]').content;

    var sign_document = new Vue({

        el: '#sign_document',
        components: {VueEditor},
        data:{

            errorCode: null,
            isLoading: false,
            text_document: null,

        },
        methods: {
            signDocument(id,text){
                //montamos la url para la consutla
                let url = SITE_URL + '/' +  MODULE_URL + '/set-contract/' + id;

                axios.post(url).then((response) => {

                    console.log(response.data);
                    //lanzamos el preload
                    this.getLoading();
                    //redireccionamos con un delay de 4 segundo
                    setTimeout(() => {

                        window.location.href = SITE_URL + '/' +  MODULE_URL + '/signed-document/' + response.data.hash + '/' + response.data.id;
      
                      }, 4000);
                    

                }).catch(error => {
                   
                    this.errorCode = error.response;
                    //lanzamos el mensaje de error
                    this.$dialog.alert(text).then(function(dialog) {
                        console.log('Closed');
                    });
                });
            },
            //lanza la barra de carga
            getLoading() {

                this.isLoading = true;
                this.$zLoading.open();

                setTimeout(() => {

                  this.$zLoading.close();
                  this.isLoading = false;

                }, 3000);

            },
            /**
              * cargamos el texto del documento
              */
             _loadData() {
                //montamos la url para la consulta
                let url = SITE_URL + '/' + MODULE_URL + '/' + 'get-document/' + DOCUMENT_ID
                //realizamos la consutla mediante ajax
                axios.get(url).then((response) => {

                    this.text_document = response.data;

                }).catch(error => {

                    this.errorCode = error.response;
                });
                //cargamos la colección de clientes
                this._getCustomers();

            }
        },
        mounted() {
            this._loadData();
        },

    });
}