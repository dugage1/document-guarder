@extends('document::layouts.master')

@section('subheader_title')
    <i class="flaticon-file-2"></i> {{ trans('app.documents.documents') }}
@stop

@section('subheader_desc')
    {{ trans('app.edit') }} {{ trans('app.documents.document') }}
@stop

@section('content')

    <div class="col-md-12">

        <div id="crete_or_update__componet" class="kt-portlet">
            
            @include('document::partials.forms.main_form')

        </div>

    </div>

@stop