@extends('document::layouts.master')

@section('subheader_title')
    <i class="flaticon-file-2"></i> {{ trans('app.documents.documents') }}
@stop

@section('subheader_desc')
    {{ trans('app.listOf') }} {{ trans('app.documents.documents') }}
@stop

@section('content')

    <div class="kt-section col-md-12">

        @if (Auth::User()->isA('customer'))
            @include('document::partials.menubars.menu_bar_customer')
        @endif

        @if (Auth::User()->isA('admin'))
            @include('document::partials.menubars.menu_bar_admin')
        @endif

    </div>

    <div id="table-data" class="kt-portlet kt-portlet--mobile">

        <div class="kt-portlet__head kt-portlet__head--lg">

            <div class="kt-portlet__head-toolbar col-xl-4 p-0">

            @if (Auth::User()->isA('admin'))

                <a href="{{ route('document.create') }}" class="btn btn-brand btn-icon-sm" aria-haspopup="true" aria-expanded="false">
                    <i class="flaticon-add-circular-button"></i>{{ trans('app.create') }} {{ trans('app.documents.newDocument') }}  	
                </a>

            @endif

            </div>

            <div class="kt-portlet__head-toolbar col-xl-3 p-0">
                
                <div class="kt-input-icon kt-input-icon--left">
                    <input v-model="searchParam" @keyup="getDataBySearchParam" name="searchParam" type="text" class="form-control" placeholder="{{ trans('app.search') }}..." id="generalSearch">
                    <span class="kt-input-icon__icon kt-input-icon__icon--left">
                        <span><i class="flaticon-search"></i></span>
                    </span>
                </div>
            
            </div>

        </div>

        <div class="kt-portlet__body">

            <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--loaded" id="local_data">
                
            @if (Auth::User()->isA('admin'))
                @include('document::partials.tables.index_table')
            @endif

            @if (Auth::User()->isA('customer'))
                @include('document::partials.tables.documents_customer_table')
            @endif
            
            </div>

        </div>

    </div>

@stop
