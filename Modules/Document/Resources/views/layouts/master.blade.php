@extends('layouts.app')

@section('meta') 

    <meta name="module-url" content="{{ Request::segment(1) }}">
    <meta name="method" content="{{ Request::segment(2) != '' ? Request::segment(2) : 'index' }}">

@stop

@section('js')

    @if( Request::segment(2) == 'create' OR Request::segment(2) == 'edit' )
        
        <script src="{{asset('/js/document_create_or_update.js')}}"></script>

    @elseif( Request::segment(2) == 'show' OR Request::segment(2) =='sign-document' )

        @if(Request::segment(2) == 'show')
            <script src="{{asset('/js/document_create_or_update.js')}}"></script>
        @endif

            <script src="{{asset('/js/permission_show_document.js')}}"></script>
            <script src="{{asset('/js/signature_document.js')}}"></script>

        <style>
            .ql-toolbar{display:none;}
            #show_document_component{border-top:1px solid #ccc;}
        </style>

    @elseif( Request::segment(2) == '' OR Request::segment(2) == 'customer')

        <script src="{{asset('/js/table_data.js')}}"></script>

    @endif

@stop