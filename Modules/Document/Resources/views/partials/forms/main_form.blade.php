<form ref="formMain" class="kt-form">
    @csrf
    <input type="hidden" name="document_id" id="document_id" value="{{ $id ?? 0 }}">

    <div class="kt-portlet__body">

        @if( Request::segment(2) == 'create' )

        <div class="form-group" :class="{'has-error': errors.has('document_number') }">
            <label>{{ trans('app.customers.customer') }}</label>
            <autocomplete 
            :source="customers" 
            placeholder="Buscar número de documento"
            results-value="document_number"
            results-display="document_number"
            input-class="form-control"
            ref="autocomplete"
            name="document_number"
            v-model="document_number"
            v-validate="'required'"
            data-vv-as="{{ trans('app.customers.customer') }}"
            >
            </autocomplete>
            <span class="alert-danger" v-text="errors.first('document_number')"></span>
        </div>

        @elseif( Request::segment(2) == 'edit' )

        <div class="form-group">
            <label>{{ trans('app.customers.customer') }}</label>
            <input disabled type="text" class="form-control" v-model="document_number">
        </div>

        @endif

        <div class="form-group" :class="{'has-error': errors.has('title') }">
            <label>{{ trans('app.title') }}</label>
            <input type="text" class="form-control" name="title" id="title" data-vv-as="{{ trans('app.title') }}" v-validate="'required'" v-model="title">
            <span class="alert-danger" v-text="errors.first('title')"></span>
        </div>

        <div class="form-group">
            <label> {{ trans('app.documents.document') }}</label>
            <vue-editor v-model="text_document"></vue-editor>
        </div>

        <div class="form-group form-group-last">
            <div class="alert alert-secondary" role="alert">
                <div class="alert-icon"><i class="flaticon-attachment"></i></div>
                <div class="alert-text">
                    {!! trans('app.attachFile') !!}
                </div>
            </div>
        </div>

    </div>

    <div class="kt-portlet__foot">

        @include('layouts.partials.forms.buttons.actions')

    </div>

</form>