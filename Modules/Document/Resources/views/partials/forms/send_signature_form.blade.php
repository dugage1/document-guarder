<div class="alert alert-secondary" role="alert">
    <div class="alert-icon"><i class="flaticon-warning kt-font-brand"></i></div>
    <div class="alert-text">
    {{ trans('app.documents.ableToSeeMessage') }}
    </div>
</div>

@if(Request::segment(2) == 'show')
<form ref="permission_show_document" id="permission_show_document"  method="POST" action="{{ route('document.show',$document->id) }}" class="kt-form kt-form--label-right">
@else
<form ref="permission_show_document" id="permission_show_document" method="POST" action="{{ route('document.sigdocument',$document->id) }}" class="kt-form kt-form--label-right">
@endif
    @csrf
    <div class="form-group row">

        <label for="example-text-input" class="col-2 col-form-label">{{ trans('app.code') }}</label>
        <div class="col-8">
            <input v-validate="'required|min:6'" data-vv-as="{{ trans('app.code') }}" class="form-control" name="code" type="text" value="" id="signature">
            <span class="alert-danger" v-text="errors.first('code')"></span>
        </div>
        <div class="col-2">
            <button @click="onSubmit" type="button" class="btn btn-success">{{ trans('app.show') }}</button>
        </div>

    </div>

</form>