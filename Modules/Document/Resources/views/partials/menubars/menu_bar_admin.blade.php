<div class="kt-section__content kt-section__content--solid">
    <a href="{{ route('document') }}" class="btn btn-outline{{ Request::segment(1) == 'document' ? '' : '-hover' }}-primary">{{ trans('app.documents.documents') }}</a>
    <a href="{{ route('contract') }}" class="btn btn-outline{{ Request::segment(1) == 'contract' ? '' : '-hover' }}-primary">{{ trans('app.documents.contracts') }}</a>
</div>