<template v-if="preloader">
    <div id="content-preloader">
        <p>
            <span>{!! trans('app.preloadDataTable') !!}</span> 
        </p>
    </div>
</template>

<template v-if="!preloader">

    <table v-if="rows.data.length > 0" class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
        <thead>
            <tr>
                <th>ID</th>
                <th>{{ trans('app.fileNumber') }}</th>
                <th>{{ trans('app.title') }}</th>
                <th>{{ trans('app.documents.document') }}</th>
                <th>{{ trans('app.registerDate') }}</th>
                <th>{{ trans('app.actions') }}</th>
            </tr>
        </thead>
        <tbody>
            <tr v-for="row in rows.data">
                <td v-text="row.id"></td>
                <td v-text="row.file_number"></td>
                <td v-text="row.title"></td>
                <td v-text="row.document_number"></td>
                <td v-text="row.created_at"></td>
                <td nowrap>
                    <a :href="'{{ url('document/edit') }}/'+row.id" class="btn btn-success btn-sm"><i class="flaticon-edit"></i></a>
                    <a @click="detroyData('{!! trans('app.dialogConfirmDestroy') !!}',row.id)" class="btn btn-dark btn-sm"><i class="flaticon-delete"></i></a>
                </td>
            </tr>
        </tbody>
    </table>

    <div v-else class="alert alert-success">

        {!! trans('app.noDataShow') !!}
       
    </div>


</template>