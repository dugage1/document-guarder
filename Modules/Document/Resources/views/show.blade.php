@extends('document::layouts.master')

@section('subheader_title')
    <i class="flaticon-file-2"></i> {{ trans('app.documents.documents') }}
@stop

@section('subheader_desc')
    {{ trans('app.show') }} {{ trans('app.documents.document') }} @if($permission) {{$document->file_number}} @endif
@stop

@section('content')

    <div class="kt-section col-md-12">

        @include('document::partials.menubars.menu_bar_customer')
        
    </div>
    
    <div class="col-md-12">

        <div id="sign_document" class="kt-portlet">
            
            <div class="kt-portlet__body">

                @if($permission)

                    <div id="show_document_component" class="form-group">
                        <input type="hidden" name="document_id" id="document_id" value="{{$id}}">
                        <vue-editor disabled v-model="text_document"></vue-editor>
                    </div>

                @else
                    @include('document::partials.forms.send_signature_form')
                @endif

            </div>

        </div>

    </div>

@stop