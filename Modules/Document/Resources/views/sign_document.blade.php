@extends('document::layouts.master')

@section('subheader_title')
    <i class="flaticon-file-2"></i> {{ trans('app.documents.documents') }}
@stop

@section('subheader_desc')
    {{ trans('app.documents.sign') }} {{ trans('app.documents.document') }} @if($permission) {{$document->file_number}} @endif
@stop

@section('content')

    <div class="kt-section col-md-12">

        @include('document::partials.menubars.menu_bar_customer')

    </div>

    <div class="col-md-12">

        <div id="crete_or_update__componet" class="kt-portlet">
            
            <div class="kt-portlet__body">

                @if($permission)

                    <div id="sign_document" class="kt-form__actions">

                        <div id="show_document_component" class="form-group">
                            <input type="hidden" name="document_id" id="document_id" value="{{$id}}">
                            <vue-editor disabled v-model="text_document"></vue-editor>
                        </div>

                        <template v-if="isLoading">
                            <button type="button" class="btn btn-primary disabled"><i class="flaticon-edit"></i> {{ trans('app.documents.signing') }} {{ trans('app.documents.document') }}</button>
                        </template>
                        <template v-else>
                            <button @click="signDocument({{$id}},'{{ trans('app.documents.errorSign') }}')" type="button" class="btn btn-primary"><i class="flaticon-edit"></i> {{ trans('app.documents.sign') }}</button>
                        </template>
                        
                    </div>
                @else
                    @include('document::partials.forms.send_signature_form')
                @endif

            </div>

        </div>

    </div>

@stop