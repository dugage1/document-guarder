@extends('document::layouts.master')

@section('subheader_title')
    <i class="flaticon-file-2"></i> {{ trans('app.documents.documents') }}
@stop

@section('subheader_desc')
    {{ trans('app.documents.document') }} {{ trans('app.documents.signed') }} | {{$contract->file_number}}
@stop

@section('content')

    <div class="kt-section col-md-12">

        @include('document::partials.menubars.menu_bar_customer')

    </div>

    <div class="kt-section col-md-12">
        <div class="alert alert-success" role="alert">
            <div class="alert-icon"><i class="flaticon-warning"></i></div>
            <div class="alert-text">{{ trans('app.documents.theDocument') }} {{$contract->file_number}} {{ trans('app.documents.hasBeenSignedCorrectly') }}.</div>
        </div>
    </div>

@stop