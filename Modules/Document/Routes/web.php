<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['web', 'auth'], 'prefix' => 'document'], function() {
    Route::get('/', 'DocumentController@index')->name('document');
    Route::get('/create', 'DocumentController@create')->name('document.create');
    Route::post('/store', 'DocumentController@store');
    Route::get('/edit/{id}', 'DocumentController@edit')->name('document.edit')->where('id', '[0-9]+');
    Route::post('/update/{id}', 'DocumentController@update')->where('id', '[0-9]+');
    Route::delete('/destroy/{id}', 'DocumentController@destroy')->name('document.destroy')->where('id', '[0-9]+');
    Route::get('/search/{query}', 'DocumentController@search');
    //rutas para DocumentCustomerController
    Route::get('/customer', 'DocumentCustomerController@index')->name('document.customer');
    Route::post('/show/{id}', 'DocumentCustomerController@show')->name('document.show')->where('id', '[0-9]+');
    Route::get('/show/{id}', 'DocumentCustomerController@show')->name('document.show')->where('id', '[0-9]+');
    Route::get('/get-document/{id}', 'DocumentCustomerController@getDocument')->where('id', '[0-9]+');
    Route::post('/sign-document/{id}', 'DocumentCustomerController@signDocument')->name('document.sigdocument')->where('id', '[0-9]+');
    Route::get('/sign-document/{id}', 'DocumentCustomerController@signDocument')->name('document.sigdocument')->where('id', '[0-9]+');
    Route::get('/signed-document/{hash}/{id}', 'DocumentCustomerController@signedDocument')->name('document.signeddocument')->where(['hash' => '[0-9a-zA-Z]+','id' => '[0-9]+']);

    Route::post('/set-contract/{id}', 'DocumentCustomerController@setContract')->where('id', '[0-9]+');
});