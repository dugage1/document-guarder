@extends('home::layouts.master')

@section('subheader_title')
    <i class="flaticon-buildings"></i> Escritorio
@stop

@section('content')
    
    @include('home::partials.widgets.customers_widget')
    @include('home::partials.widgets.documents_widget')
    @include('home::partials.widgets.alerts_widget')

@stop
