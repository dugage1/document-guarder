<div class="col-xl-12">
		
    <div class="kt-portlet kt-portlet--height-fluid">

        <div class="kt-widget14">

            <div class="kt-widget14__header kt-margin-b-30">
                <a href="{{ url('/inbox') }}">
                    <h3 class="kt-widget14__title">
                        <i class="flaticon-alert"></i> {{ trans('app.alerts.alerts') }}              
                    </h3>
                </a>
                <span class="kt-widget14__desc">
                    {{ trans('app.alerts.details') }}
                </span>
            </div>

            <div class="kt-widget14__content">	
                
            </div>

        </div>

    </div>	

</div>