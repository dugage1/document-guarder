<?php

namespace Modules\Inbox\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Auth;
use App\Models\InboxReceived;
use App\Models\InboxSent;
use App\Models\UserData;

class InboxController extends Controller
{
    private $paginate = 0;

    function __construct()
    {
        $this->paginate = Config::get('app.pagesNumber');
    }
    /**
     * show received message list
     * @return Response
     */
    public function showReceivedMessagesList()
    {
        if( request()->ajax() ) {

            $messages = InboxReceived::where('to_id',Auth::id())
            ->orderBy('id','desc')
            ->paginate($this->paginate);
            //retornamos el resultado como json
            return response()->json($messages);

        }else{

            return view('inbox::index');
            
        }
        
    }

    /**
     * show sent message list
     * @return Response
     */
    public function showSentMessagesList()
    {
        if( request()->ajax() ) {

            $messages = InboxSent::where('from_id',Auth::id())
            ->orderBy('id','desc') 
            ->paginate($this->paginate);
            //retornamos el resultado como json
            return response()->json($messages);

        }else{

            return view('inbox::index');
            
        }
    }

    /**
     * Show the form for creating a new resource.
     * Este metodo puede aceptar dos parametros, id y method
     * en caso de ser distintos de 0 y null, indicara, que 
     * el mesaje puede ser un responder o reenviar, y consultará
     * la tabla correspondiente para obtener los datos del mensaje
     * @return Response
     */
    public function create($id = 0,$method = null)
    {
        //comprobamos si id y method son distintos de id y null
        //convertimos el parametro method en un vector    
        $id > 0 ? $method = explode('_',$method) : '';

        if( request()->ajax() ) {

            return $this->show($id,$method[0],$method[1]);

        }else{

            //retornnamos los datos a la vista
            return view('inbox::create',compact('id','method'));
            
        }
        
    }

    /**
     * Store a newly created resource in storage.
     * recolectamos los datos por post y cremos
     * en sendas tablas inbox los registros
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //obtenemos los datos del usuario al que vamos a enviar
        $userData = UserData::with('user')
                        ->where('document_number',$request->document_number)
                        ->first();
        //instanciamos inboxs
        $inboxSent = new InboxSent;
        $inboxReceived = new InboxReceived;
        //seteamos los datos del sent
        $inboxSent->from_id = Auth::id();
        $inboxSent->to_id = $userData->user->id;
        $inboxSent->from = Auth::user()->email;
        $inboxSent->to = $userData->user->email;
        $inboxSent->subject = $request->subject;
        $inboxSent->body = $request->body;
        //guardamos, comprobamos y si se guardo, 
        //registramos el received.
        if( $inboxSent->save() ) {
            
            $inboxReceived->from_id = Auth::id();
            $inboxReceived->to_id = $userData->user->id;
            $inboxReceived->from = Auth::user()->email;
            $inboxReceived->to = $userData->user->email;
            $inboxReceived->parent_id = $inboxSent->id;
            $inboxReceived->subject = $request->subject;
            $inboxReceived->body = $request->body;
            $inboxReceived->save();
        }

        //retornamos el registro creado
        return response()->json($inboxSent);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id,$segment = null,$action = null)
    {
        //consultamos la table según sea sent or received
        //obtenemos los datos del mensaje por id
        if( request()->segment(2) == 'show-sent-message' OR $segment == 'show-sent-message' ) {

            $message = InboxSent::find($id);

        }elseif( request()->segment(2) == 'show-received-message' OR $segment == 'show-received-message' ){

            $message = InboxReceived::with('userData')->find($id);
            
        }
        //si message es vacío, etonces estamos en modo crear, y le pasamos
        //un objeto con los parametros vacios
        if( empty($message) ) {

            $message = $this->_getDefaultResult();

        }else{
            //si el mensaje esta como no leido, lo pasamos a leido
            //en ambas tablas
            if( $message->read == 0 ) {

                $received = InboxReceived::find($id);
                $received->read = 1;
                $received->save();
                $sent = InboxSent::find($received->parent_id);
                if( $sent ) {

                    $sent->read = 1;
                    $sent->save();
                }
            }
        }

        if( request()->ajax() ) {
            //retornamos el resultado como json
            return response()->json($message);

        }else{

            return view('inbox::show',compact('message','id'));
        }
    }

    /**
     * retorna los datos del usuario por 
     * número de documento
     */
    public function getUserData($documentNumber)
    {
        if( request()->ajax() ) {

            //obtenemos los datos del usuario
            $userData = UserData::with('user')
                        ->where('document_number',$documentNumber)
                        ->first();
            //retornamos el resultado como json
            return response()->json($userData);

        }else{

            abort(404);
            
        }
    }
    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        if( request()->ajax() ) {
            //comprobamos desde que vista realizamos la acción
            //según la ruta, y borramos de la tabla correspondiente
            if( request()->segment(2) == 'received-messages' OR request()->segment(2) == 'show-received-message' ) {

                InboxReceived::destroy($id);

            }elseif( request()->segment(2) == 'sent-messages' OR request()->segment(2) == 'show-sent-message' ){

                InboxSent::destroy($id);
            }
            

        }else{

            abort(404);
        }
    }

    /**
     * Método encargado de realizar las consultas en forma de búsqueda y retornar el resultado 
     * @param query -- parametro encargado de almacenar los datos pasados por el campo
     * 
     */
    public function search($query)
    {
        if( request()->ajax() ) {

             //comprobamos desde que vista realizamos la acción
            //según la ruta, y borramos de la tabla correspondiente
            if( request()->segment(3) == 'received-messages') {
                
                $messages = InboxReceived::where('from', 'LIKE', '%'.$query.'%')
                ->orWhere('subject','LIKE','%' . $query . '%')
                ->paginate($this->paginate);

            }elseif( request()->segment(3) == 'sent-messages' ){

                $messages = InboxSent::where('to', 'LIKE', '%'.$query.'%')
                ->orWhere('subject','LIKE','%' . $query . '%')
                ->paginate($this->paginate);
            }

            return response()->json($messages);

        }else{

            abort(404);
        }
    }

    /**
     * get default object if empty
     * @return Response
     */
    private function _getDefaultResult($test = false) 
    {
        $obj = [
            
            'to' => '',
            'subject' => '',
            'body' => '',
            
        ];
        return (object)$obj;
    }
}