/**PAQUETES UTILIZADOS*/

/**
 * Vuejs Dialog Plugin | Para los mensajes en las acciones
 */
import VuejsDialog from "vuejs-dialog";
import VuejsDialogMixin from "vuejs-dialog/dist/vuejs-dialog-mixin.min.js";
import 'vuejs-dialog/dist/vuejs-dialog.min.css';
//pasamos la configuración lara VuejsDialog
Vue.use(VuejsDialog, {
    html: true,
    loader: false,
    okText: 'Continuar',
    cancelText: 'Cancelar',
    animation: 'bounce'
});

/***************************************************/

const SITE_URL = document.head.querySelector('meta[name="site-url"]').content;
const MODULE_URL = document.head.querySelector('meta[name="module-url"]').content;
const METHOD = document.head.querySelector('meta[name="method"]').content;
const INBOX_ID = document.querySelector('#inbox_id').value;

if (document.querySelector('#kt-section__content')) {

    var kt_section__content = new Vue({

        el: '#kt-section__content',
        data:{

            errorCode: false,
            isLoading: false,

        },
        methods: {
            /**
             * Método encargado de eliminar de forma definitiva el datos mediante su ID
             * Este método solicita medinate VuejsDialog la confirmación nates de realizar la acción
             * en caso positivo, llama de forma interna al metodo _destroy, que es quien realiza
             * la accion de destroy realmente.
             * @param id -- id del registro a borrar
             */
            detroyData: function(text,id){
                //solicitamos al usuario que confirme la acción
                this.$dialog.confirm(text)
                .then((dialog) => {

                   this._destroy(id);

                })
                .catch(function () {

                    console.log('Clicked on cancel');

                });
                
            },
            /**
             * Elimina de forma definitiva el dato mediante su id
             * @param url -- almacena la url para la consulta
             * @param id -- id del datos a borrar
             */
            _destroy(id){

                //montamos la url para la consulta
                let url = SITE_URL+'/'+MODULE_URL+ '/' + METHOD + '/destroy/'+id;
                //mediante ajax realizamos la consulta para destruir el dato
                axios.delete(url).then((response) => {
                    //retornamos al listado de mensajes
                    location.href = SITE_URL + '/' + MODULE_URL;

                }).catch(error => {
                    this.errorCode = error.response;
                });
            },

        }

    });
}