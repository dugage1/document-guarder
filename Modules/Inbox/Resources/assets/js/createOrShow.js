/**PAQUETES UTILIZADOS*/

import VeeValidate from 'vee-validate';
//cargamos idioma español para las validaciones
const VueValidationEs = require('vee-validate/dist/locale/es');
//y le pasamos la constante a la configuración
const config = {
    locale: 'es',
    events: 'blur',
    dictionary: {
        es: VueValidationEs

    }
};
//instanciamos vee validate
Vue.use(VeeValidate,config);

//compnente para editor de texto
import {VueEditor} from "vue2-editor";

//Paquete para campos de autocompletado
import Autocomplete from 'vuejs-auto-complete';

//paqueta para la barra de carga de datos
import Vue from 'vue';
import zloading from 'z-loading';
import 'z-loading/dist/z-loading.css';
Vue.use(zloading);

/***************************************************/

const SITE_URL = document.head.querySelector('meta[name="site-url"]').content;
const MODULE_URL = document.head.querySelector('meta[name="module-url"]').content;
const METHOD = document.head.querySelector('meta[name="method"]').content;
const INBOX_ID = document.querySelector('#inbox_id').value;
const INBOX_METHOD = document.querySelector('#inbox_method').value;
const INBOX_ACTION = document.querySelector('#inbox_action').value;

if (document.querySelector('#crete_or_show__componet')) {

    var crete_or_show__componet = new Vue({

        el: '#crete_or_show__componet',
        components: {VueEditor,Autocomplete},
        data:{

            errorCode: null,
            erroValidate: false,
            isLoading: false,
            document_number: null,
            userData: [],
            customers: [],
            data:[],
            saved: false,
            routes: [],
            inboxMethod: INBOX_METHOD,
            inboxAction: INBOX_ACTION,

        },
        methods: {
            /**
             * Guarda el las dos tablas inbox
             * el mesaje enviado
             */
            saveData : function() {
                //sobreescribimos errorCode a null
                this.errorCode = null;
                //url de la consutla
                let url = SITE_URL + '/' +  MODULE_URL + '/store';
                //capturamos todos los campos de formulario
                const data = {};
                //creamos el objeto
                Object.assign(data, { 
                    'document_number': this.document_number, 
                    'to': this.data.to,
                    'subject': this.data.subject,
                    'body': this.data.body,
                })
                //console.log(data);
                this.$validator.validateAll().then(() => {
                       
                    if ( !this.errors.any() ) {
                        
                        axios.post(url,data).then((response) => {

                            //console.log(response);
                            this.getLoading();
                            //saved = true para mostrar la botonera alternativa
                            this.saved = true;
                            //montamos las rutas necesarias
                            this.routes = {
                                index: SITE_URL + '/' +  MODULE_URL, 
                                create: SITE_URL + '/' +  MODULE_URL + '/' + METHOD,
                            };

                        }).catch(error => {

                            this.errorCode = error.response;
                        });

                    }else{

                        this.erroValidate = true;
                    }

                });
            },
            //lanza la barra de carga
            getLoading() {

                this.isLoading = true;
                this.$zLoading.open();

                setTimeout(() => {

                  this.$zLoading.close();
                  this.isLoading = false;

                }, 3000);

            },
            /**
             * retorna los datos del usuario selecciona
             * por su DNI, y sobreescribimos data.to para mostrar el
             * email en la vista del destinatario.
             */
            getUserData(){
                //montamos la url para la consulta
                let url = url = SITE_URL + '/' + MODULE_URL + '/get-user-data/' + this.document_number;
                //realizamos la consutla mediante ajax
                axios.get(url).then((response) => {
                    //console.log(response.data);
                    this.userData = response.data;
                    this.data.to = this.userData.user.email;

                }).catch(error => {

                    this.errorCode = error.response;
                });
            },
            /**
             * monta las rutas necesarias para las botoneras secundarias
             */
            _buildRoutes(id = 0){
                //creamos el objeto
                let routes = [];
                Object.assign(routes, { 
                    'index': SITE_URL + '/' +  MODULE_URL, 
                    'create': SITE_URL + '/' +  MODULE_URL + '/' + METHOD,
                });
                //le pasamos el objeto
                //console.log(routes);
                return this.routes = routes;
            },
            /**
             * Componente privado que carga la lista de los números de documentos
             * de los clientes para el campo con autocompletado
             */
            _getCustomers(){
                //montamos la url para la consulta
                let url = SITE_URL + '/customer/get-all-customers';
                //realizamos la consutla mediante ajax
                axios.get(url).then((response) => {
                    //console.log(response.data);
                    this.customers = response.data;

                }).catch(error => {

                    this.errorCode = error.response;
                });
                
            },
            /**
              * consutla y carga los parametros del formulario, estos pueden ser vacios
              * para create o con datos en caso de show.
              * 
              */
             _loadData() {
                //montamos la url para la consulta
                let inboxData = ''; INBOX_METHOD;
                INBOX_METHOD != '' && INBOX_ACTION != '' ?  inboxData = '/' + INBOX_METHOD + '_' + INBOX_ACTION : '';
                let url = SITE_URL + '/' + MODULE_URL + '/' + METHOD + '/' + INBOX_ID + inboxData;
                //si inbox == 0 sobreescribimos la url
                INBOX_ID == 0 ? url = SITE_URL + '/' + MODULE_URL + '/show-received-message/' + INBOX_ID : '';
                //realizamos la consutla mediante ajax
                axios.get(url).then((response) => {
                    //console.log(response);
                    //si el INBOX_ACTION == resend, limpiamos response.data.to;
                    INBOX_ACTION == 'resend' ?  response.data.to = null : '';
                    this.data = response.data;
                    this.document_number = response.data.user_data.document_number;

                }).catch(error => {

                    this.errorCode = error.response;
                });
                //cargamos la colección de clientes
                this._getCustomers();

            }
            
        },
        mounted() {

            this._loadData();
        }

    });

}