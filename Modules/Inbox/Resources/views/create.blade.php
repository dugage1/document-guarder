@extends('inbox::layouts.master')

@section('subheader_title')
    <i class="flaticon-multimedia-2"></i> {{ trans('app.inbox.messages') }}
@stop

@section('subheader_desc')

    @if( $method[1] == 'reply' )
        {{ trans('app.inbox.reply') }}
    @elseif( $method[1] == 'resend' )
        {{ trans('app.inbox.resend') }}
    @else
        {{ trans('app.new') }} {{ strtolower(trans('app.inbox.message')) }}
    @endif

@stop

@section('content')
    
    <div class="col-md-12">

        <div id="crete_or_show__componet" class="kt-portlet">
            
            @include('inbox::partials.forms.main_form')

        </div>

    </div>

@stop