@extends('inbox::layouts.master')

@section('subheader_title')
    <i class="flaticon-multimedia-2"></i> {{ trans('app.inbox.messages') }}
@stop

@section('subheader_desc')

    @if( Request::segment(2) == '')

        {{ trans('app.listOf') }} {{ strtolower(trans('app.inbox.receivedMessages')) }}

    @elseif( Request::segment(2) == 'sent-messages' )

        {{ trans('app.listOf') }} {{ strtolower(trans('app.inbox.sentMessages')) }}

    @endif
    
@stop

@section('content')

    <div class="kt-section col-md-12">

        @include('inbox::partials.menubars.menu_bar')

    </div>

    <div id="table-data" class="kt-portlet kt-portlet--mobile">

        <div class="kt-portlet__head kt-portlet__head--lg">

            <div class="kt-portlet__head-toolbar col-xl-4 p-0">

                <a href="{{ route('inbox.create') }}" class="btn btn-brand btn-icon-sm" aria-haspopup="true" aria-expanded="false">
                    <i class="flaticon-add-circular-button"></i>{{ trans('app.create') }} {{ trans('app.inbox.newMessage') }}  	
                </a>

            </div>

            <div class="kt-portlet__head-toolbar col-xl-3 p-0">
                
                <div class="kt-input-icon kt-input-icon--left">
                    <input v-model="searchParam" @keyup="getDataBySearchParam" name="searchParam" type="text" class="form-control" placeholder="{{ trans('app.search') }}..." id="generalSearch">
                    <span class="kt-input-icon__icon kt-input-icon__icon--left">
                        <span><i class="flaticon-search"></i></span>
                    </span>
                </div>
            
            </div>

        </div>

        <div class="kt-portlet__body">

            <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--loaded" id="local_data">
                
            @if (Request::segment(2) == '')
                @include('inbox::partials.tables.received_messages_table')
            @endif

            @if ( Request::segment(2) == 'sent-messages' )
                @include('inbox::partials.tables.sent_messages_table')
            @endif
            
            </div>

        </div>

    </div>

@stop

