@extends('layouts.app')

@section('meta') 

    <meta name="module-url" content="{{ Request::segment(1) }}">
    <meta name="method" content="{{ Request::segment(2) != '' ? Request::segment(2) : 'received-messages' }}">

@stop

@section('js')

    @if( Request::segment(2) == '' || Request::segment(2) == 'sent-messages' )

        <script src="{{asset('/js/table_data.js')}}"></script>

    @endif

    @if( Request::segment(2) == 'create' )

        <script src="{{asset('/js/create_or_show.js')}}"></script>

    @endif

    @if( Request::segment(2) == 'show-sent-message' || Request::segment(2) == 'show-received-message')

        <script src="{{asset('/js/create_or_show.js')}}"></script>
        <script src="{{asset('/js/buttons_bar.js')}}"></script>

        <style>
            .ql-toolbar{display:none;}
            #show_contract_component{border-top:1px solid #ccc;}
        </style>

    @endif

@stop