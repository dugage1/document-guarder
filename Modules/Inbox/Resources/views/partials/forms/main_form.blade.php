<form ref="formMain" class="kt-form">
    @csrf
    <input type="hidden" name="inbox_id" id="inbox_id" value="{{ $id ?? 0 }}">
    <input type="hidden" name="inbox_method" id="inbox_method" value="{{ $method[0] ?? '' }}">
    <input type="hidden" name="inbox_action" id="inbox_action" value="{{ $method[1] ?? '' }}">
    

    <div class="kt-portlet__body">

        @if( Request::segment(2) == 'create' )

            <div v-if="inboxAction == 'reply'" class="form-group">
                <input disabled type="text" class="form-control" name="document_number"  v-model="document_number">
            </div>

            <div v-else  class="form-group" :class="{'has-error': errors.has('document_number') }">
                <label>{{ trans('app.customers.customer') }}</label>
                <autocomplete 
                :source="customers"
                placeholder="Buscar número de documento"
                results-value="document_number"
                results-display="document_number"
                input-class="form-control"
                ref="autocomplete"
                name="document_number"
                v-model="document_number"
                v-validate="'required'"
                data-vv-as="{{ trans('app.customers.customer') }}"
                @close="getUserData"
                >
                </autocomplete>
                <span class="alert-danger" v-text="errors.first('document_number')"></span>
            </div>

            <div v-if="inboxAction == 'reply'" class="form-group">
                <input disabled type="text" class="form-control" name="to"  v-model="data.from">
            </div>

            <div v-else class="form-group">
                <input disabled type="text" class="form-control" name="to"  v-model="data.to">
            </div>

            <div class="form-group" :class="{'has-error': errors.has('subject') }">
                <label>{{ trans('app.inbox.subject') }}</label>
                <input type="text" class="form-control" name="subject" id="subject" data-vv-as="{{ trans('app.inbox.subject') }}" v-validate="'required'" v-model="data.subject">
                <span class="alert-danger" v-text="errors.first('subject')"></span>
            </div>

            <div class="form-group">
                <label> {{ trans('app.text') }}</label>
                <vue-editor v-model="data.body"></vue-editor>
            </div>

        @else

        <p><span v-text="data.subject"></span></p>
        <h5 v-text="data.from"></h5>
        <span v-html="data.body" class="kt-section__info"></span>

        @endif

    </div>

    @if( Request::segment(2) != 'show-received-message' AND Request::segment(2) != 'show-sent-message' )

        <div class="kt-portlet__foot">

            @include('layouts.partials.forms.buttons.actions')

        </div>

    @endif

</form>