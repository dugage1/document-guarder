<div class="kt-section__content kt-section__content--solid">
    <a href="{{ route('inbox') }}" class="btn btn-outline{{ Request::segment(2) == '' ? '' : '-hover' }}-primary">{{ trans('app.inbox.receivedMessages') }}</a>
    <a href="{{ route('inbox.sentmessages') }}" class="btn btn-outline{{ Request::segment(2) == 'sent-messages' ? '' : '-hover' }}-primary">{{ trans('app.inbox.sentMessages') }}</a>
</div>