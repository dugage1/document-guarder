<div id="kt-section__content" class="kt-section__content kt-section__content--solid">
    <a href="{{ route('inbox.create',[$id,Request::segment(2).'_resend']) }}" class="btn btn-outline{{ Request::segment(2) == '' ? '' : '-hover' }}-primary"><i style="color:#000" class="flaticon-email"></i> {{ trans('app.inbox.resend') }}</a>
    @if (Request::segment(2) == 'show-received-message')
        <a href="{{ route('inbox.create',[$id,Request::segment(2).'_reply']) }}" class="btn btn-outline{{ Request::segment(2) == '' ? '' : '-hover' }}-primary"><i style="color:#000" class="flaticon-reply"></i> {{ trans('app.inbox.reply') }}</a>
    @endif
    <a @click="detroyData('{!! trans('app.dialogConfirmDestroy') !!}',{{$id}})" class="btn btn-outline{{ Request::segment(2) == 'sent-messages' ? '' : '-hover' }}-primary"><i style="color:#000" class="flaticon-delete"></i> {{ trans('app.delete') }}</a>
</div>