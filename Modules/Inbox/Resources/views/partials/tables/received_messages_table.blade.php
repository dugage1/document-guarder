<template v-if="preloader">
    <div id="content-preloader">
        <p>
            <span>{!! trans('app.preloadDataTable') !!}</span> 
        </p>
    </div>
</template>

<template v-if="!preloader">

    <table v-if="rows.data.length > 0" class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
        <thead>
            <tr>
                <th>ID</th>
                <th>{{ trans('app.inbox.subject') }}</th>
                <th>{{ trans('app.inbox.fromSet') }}</th>
                <th>{{ trans('app.date') }}</th>
            </tr>
        </thead>
        <tbody>
            <tr v-for="row in rows.data">
                <td v-text="row.id"></td>
                <td v-if="row.read == 1" v-text="row.subject"></td>
                <td v-else><strong v-text="row.subject"></strong></td>
                <td v-text="row.from"></td>
                <td v-text="row.created_at"></td>
                <td nowrap>
                    <a :href="'{{ url('inbox/show-received-message') }}/'+row.id" class="btn btn-success btn-sm"><i class="flaticon-eye"></i></a>
                    <a @click="detroyData('{!! trans('app.dialogConfirmDestroy') !!}',row.id)" class="btn btn-dark btn-sm"><i class="flaticon-delete"></i></a>
                </td>
            </tr>
        </tbody>
    </table>

    <div v-else class="alert alert-success">

        {!! trans('app.noDataShow') !!}
       
    </div>


</template>