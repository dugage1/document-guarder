@extends('inbox::layouts.master')

@section('subheader_title')
    <i class="flaticon-multimedia-2"></i> {{ trans('app.inbox.messages') }}
@stop

@section('subheader_desc')

    {{ $message->subject }}
        
@stop

@section('content')

    @if (Request::segment(2) == 'show-received-message')
        @include('inbox::partials.shows.received_message_show')
    @endif

    @if (Request::segment(2) == 'show-sent-message')
        @include('inbox::partials.shows.sent_message_show')
    @endif

@stop