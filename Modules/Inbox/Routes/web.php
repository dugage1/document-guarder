<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => ['web', 'auth'], 'prefix' => 'inbox'], function() {
    Route::get('/', 'InboxController@showReceivedMessagesList')->name('inbox');
    Route::get('/received-messages', 'InboxController@showReceivedMessagesList');
    Route::get('/create/{id?}/{method?}', 'InboxController@create')->name('inbox.create')->where('id', '[0-9]+');
    Route::post('/store', 'InboxController@store');
    Route::get('/sent-messages', 'InboxController@showSentMessagesList')->name('inbox.sentmessages');
    Route::get('/show-received-message/{id}', 'InboxController@show')->name('inbox.show')->where('id', '[0-9]+');
    Route::get('/show-sent-message/{id}', 'InboxController@show')->name('inbox.show')->where('id', '[0-9]+');
    Route::get('/get-user-data/{documentNumber}', 'InboxController@getUserData')->name('inbox.show')->where('documentNumber', '[0-9 A-Z]+');
    Route::delete('/received-messages/destroy/{id}', 'InboxController@destroy')->name('inbox.destroy')->where('id', '[0-9]+');
    Route::delete('/sent-messages/destroy/{id}', 'InboxController@destroy')->name('inbox.destroy')->where('id', '[0-9]+');
    Route::delete('/show-received-message/destroy/{id}', 'InboxController@destroy')->name('inbox.destroy')->where('id', '[0-9]+');
    Route::delete('/show-sent-message/destroy/{id}', 'InboxController@destroy')->name('inbox.destroy')->where('id', '[0-9]+');
    Route::get('/search/received-messages/{query}', 'InboxController@search');
    Route::get('/search/sent-messages/{query}', 'InboxController@search');
});
