<?php

namespace Modules\Myprofile\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Myprofile\Http\Requests\MyprofileUpdateRequest;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use App\User;

class MyprofileController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        //obtenemos los datos del usuario
        $user = User::with('user_data')->findOrFail(Auth::id());

        if( request()->ajax() ) {

            return response()->json($user);

        }else{

          return view('myprofile::index');
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(MyprofileUpdateRequest $request, $id)
    {
        //obtenemos los datos del usuario mediante su id
        $user = User::with('user_data')->findOrFail($id);
        //seteamos los datos
        $user->email = $request->email;
        $user->user_data->telephone = $request->telephone;
        !empty( $request->password ) ? $user->password = bcrypt($request->password) : '';
        //guardamos los datos en las dos tablas
        $user->save();
        $user->user_data->save();

    }

}
