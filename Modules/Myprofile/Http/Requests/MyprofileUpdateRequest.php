<?php

namespace Modules\Myprofile\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class MyprofileUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->id;

        return [

            'email' => 'required|unique:users,email,'.$id.',id',
            'password' => '',
            'telephone' => 'required|min:9',
            
        ];
    }
}