/**PAQUETES UTILIZADOS*/

import VeeValidate from 'vee-validate';
//cargamos idioma español para las validaciones
const VueValidationEs = require('vee-validate/dist/locale/es');
//y le pasamos la constante a la configuración
const config = {
    locale: 'es',
    events: 'blur',
    dictionary: {
        es: VueValidationEs

    }
};
//instanciamos vee validate
Vue.use(VeeValidate,config);
// Vue mask, para las mascaras de los campos
const VueInputMask = require('vue-inputmask').default;
Vue.use(VueInputMask);

//paqueta para la barra de carga de datos
import zloading from 'z-loading';
import 'z-loading/dist/z-loading.css';
Vue.use(zloading);


const SITE_URL = document.head.querySelector('meta[name="site-url"]').content;
const MODULE_URL = document.head.querySelector('meta[name="module-url"]').content;
const CUSTOMER_ID = document.querySelector('#customer_id').value;
/**
 * componente que maneja todo lo referente al formulario del perfil del usuario
 */
if (document.querySelector('#my_profile__component')) {

    var my_profile__component = new Vue({

        el: '#my_profile__component',
            data:{
    
                formFields: [],
                errorCode: null,
                erroValidate: false,
                password: null,
                isLoading: false,
    
            },
            methods: {
                //método para crear o actualizar los datos
                saveData : function() {
                    //sobreescribimos errorCode a null
                    this.errorCode = null;
                    //url de la consutla
                    let url = SITE_URL + '/' +  MODULE_URL + '/update/' + CUSTOMER_ID;
                    //capturamos todos los campos de formulario
                    const formData = new FormData(this.$refs['formMain']);
                    const data = {};

                    for (let [key, val] of formData.entries()) {
                        Object.assign(data, { [key]: val })
                    }
                    //realizamos la acción
                    this.$validator.validateAll().then(() => {
                       
                        if ( !this.errors.any() ) {
                            
                            axios.post(url,data).then((response) => {
    
                                console.log(response);
                                this.getLoading();
    
                            }).catch(error => {
    
                                this.errorCode = error.response;
                            });
    
                        }else{
    
                            this.erroValidate = true;
                        }
    
                    });
                },
                //lanza la barra de carga
                getLoading() {

                    this.isLoading = true;
                    this.$zLoading.open();

                    setTimeout(() => {

                    this.$zLoading.close();
                    this.isLoading = false;

                    }, 3000);

                },
                //consutla y carga los parametros del formulario.
                _loadData() {
                    //montamos la url para la consulta
                    let url = SITE_URL + '/' + MODULE_URL;
                    //realizamos la consutla mediante ajax
                    axios.get(url).then((response) => {

                        this.formFields = response.data;

                    }).catch(error => {

                        this.errorCode = error.response;
                    });

                }
            },
            mounted() {

                this._loadData();
            },
    });

}