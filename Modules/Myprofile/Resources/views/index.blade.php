@extends('myprofile::layouts.master')

@section('subheader_title')
    <i class="flaticon-customer"></i> {{ trans('app.customers.myProfile') }}
@stop

@section('subheader_desc')
    {{ trans('app.customers.editYourProfile') }}
@stop

@section('content')

    <div class="col-md-12">

        <div id="my_profile__component" class="kt-portlet">
            
            @include('myprofile::partials.forms.main_form')

        </div>

    </div>

@stop
