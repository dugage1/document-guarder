@extends('layouts.app')

@section('meta') 

    <meta name="module-url" content="{{ Request::segment(1) }}">
    <meta name="method" content="{{ Request::segment(2) != '' ? Request::segment(2) : 'index' }}">

@stop

@section('js')

    <script src="{{asset('/js/my_profile.js')}}"></script>

@stop
