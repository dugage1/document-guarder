<form ref="formMain" class="kt-form">

    @csrf

    <input type="hidden" name="customer_id" id="customer_id" value="{{ Auth::user()->id }}">

    <div class="kt-portlet__body">

        <div class="form-group" :class="{'has-error': errors.has('telephone') }">
            <label>{{ trans('app.telephone') }}</label>
            <input type="text" class="form-control" name="telephone" id="telephone" data-vv-as="{{ trans('app.telephone') }}" v-mask="{mask: '999 999 999', autoUnmask: true}" v-validate="'required|numeric|min:9|max:9'" v-model="formFields.user_data.telephone">
            <span class="alert-danger" v-text="errors.first('telephone')"></span>
        </div>

        <div class="form-group" :class="{'has-error': errors.has('email') }">
            <label>Email</label>
            <input type="text" class="form-control" name="email" id="email" v-validate="'required|email'" v-model="formFields.email">
            <span class="alert-danger" v-text="errors.first('email')"></span>
        </div>

        <div class="form-group" :class="{'has-error': errors.has('password') }">
            <label>{{ trans('app.password') }}</label>
            <input type="password" class="form-control" name="password" id="password" data-vv-as="{{ trans('app.password') }}" v-validate="'min:8'" v-model="password">
            <span class="alert-danger" v-text="errors.first('password')"></span>
        </div>

    </div>

    <div class="kt-portlet__foot">

        @include('layouts.partials.forms.buttons.actions')

    </div>

</form>