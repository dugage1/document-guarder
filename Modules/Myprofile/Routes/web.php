<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['web', 'auth'], 'prefix' => 'myprofile'], function() {
    Route::get('/', 'MyprofileController@index')->name('myprofile');
    Route::post('/update/{id}', 'MyprofileController@update')->where('id', '[0-9]+');
});
