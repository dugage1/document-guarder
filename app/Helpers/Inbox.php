<?php
namespace App\Helpers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
/**
 * Colección de metodos para interactuar con Inbox como por ejemplo
 * Cantidad de mensajes no leidos
 */
Class Inbox
{
    /**
     * retorna el total de mensajes marcados como read = 0
     * @param $messagesNoRead el total de mensajes no leidos
     * Consultamos siempre la tabla inbox_receiveds
     */
    static function getNumMessagesNotRead()
    {
        //realizamos la consulta
        $numMessagesNoRead = DB::table('inbox_receiveds')
                ->where('to_id',Auth::id())
				->where('read',0)
                ->count();
        //retornamos el total de mensajes
        return $numMessagesNoRead;

    }
}