<?php

namespace App\Http\Controllers\Blockchain;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
/**
 * 
 */
class BlockchainController extends Controller
{
    //public $nonce = 0;
    public $block = [];

    public function __construct($index = 0, $timestamp = 0, $data = null, $previousHash = 0, $nonce = 0, $otherParam = null)
    {
        if( $index == 0 ) {

             $this->block = [$this->_createGenesisBlock()];

        }else{
            
            $this->block = [$this->_createBlock($index,$timestamp,$data,$previousHash,$nonce,$otherParam)];
        }
    }

    private function _createBlock($index, $timestamp, $data, $previousHash = 0,$nonce = 0,$otherParam = null)
    {
        $this->index = $index;
        $this->nonce = $nonce;
        $this->timestamp = $timestamp;
        $this->data = $data;
        $this->previousHash = $previousHash;
        $this->hash = $this->_calculateHash($otherParam);
    }

    private function _createGenesisBlock()
    {
        return $this->_createBlock(0, strtotime('now'), "Genesis-Block-".strtotime('now'));
    }

    private function _calculateHash($otherParam = null)
    {
        if( $otherParam == null ) {

            $block = $this->index.$this->previousHash.$this->timestamp.$this->data.$this->nonce;
        
        }else{

            $block = $this->index.$this->previousHash.$this->timestamp.$this->data.$otherParam.$this->nonce;
        }

	    $hash = $this->_getHash($block);
        return $hash;
    }
 
    private function _getHash($block) 
    {
        return hash("sha256", $block);
    }
}
