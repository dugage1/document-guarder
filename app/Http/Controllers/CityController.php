<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Province;
use App\Models\City;

class CityController extends Controller
{
    public function getCitiesByProvince(Request $request)
    {

        if( request()->ajax() ) {
            //obtenemos el code de la provincia
            $province = Province::select('code')
            ->where('name',$request->input('province'))
            ->first();
            //obtenemos la colección de municipios
            $cities = City::where('code',$province->code)->get();
            return response()->json([$cities,$province]);
            
        }else{

            abort(404);
        }
    }
}
