<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendSignatureDocument extends Mailable
{
    use Queueable, SerializesModels;
    private $document = [];
    private $userData = [];
    private $code = null;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($document,$userData,$code)
    {
        $this->document = $document;
        $this->userData = $userData;
        $this->code = $code;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $document = $this->document;
        $userData = $this->userData;
        $code = $this->code;

        return $this->from('blockchain@programacionblockchain.com')
        ->view('mails.send_signature_document',compact('document','userData','code'))
        ->text('mails.send_signature_document_plain');
    }
}
