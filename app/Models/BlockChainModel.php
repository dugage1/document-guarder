<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;

class BlockChainModel extends Model
{
    protected $table = 'block_chains';
    protected $appends = ['data_decrypt'];

    //Accesors
    /*public function getDataAttribute($value)
    {
        return decrypt($value);
    }*/
    public function getDataDecryptAttribute()
    {
        return decrypt($this->data);
    }

}
