<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;

class Document extends Model
{
    // Mutators
    public function setCodeAttribute($value)
    {
        $this->attributes['code'] = bcrypt($value);
    }

    public function setTextDocumentAttribute($value)
    {
        $this->attributes['text_document'] = encrypt($value);
    }

    public function setImageAttribute($value)
    {
        $this->attributes['image'] = encrypt($value);
    }
    //método que genera y guarda el número de documento medinate id
    static function setFileNumber($id)
    {
        //obtenemos la oferta
        $document = Document::find($id);
        //numOffer donde almacenamos el número creado
        $fileNumber = 'DOC-'.date('y').'-'.strtoupper(str_random(3)).'-'.$id;
        //editamos la oferta guardando el número de oferta
        $document->file_number = $fileNumber;
        $document->save();

    }


}
