<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InboxReceived extends Model
{
    //relaciones
    public function userData()
    {
        return $this->hasOne('App\Models\UserData','user_id','from_id');
    }

}
