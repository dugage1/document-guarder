<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InboxSent extends Model
{
    //relaciones
    public function userData()
    {
        return $this->hasOne('App\Models\UserData','user_id','to_id');
    }
}
