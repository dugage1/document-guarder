<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;

class UserData extends Model
{
    //formateamos los datos necesarios
    public function setTelephoneAttribute($value)
    {
        $this->attributes['telephone'] = encrypt(str_replace(' ', '', $value));
    }

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = encrypt($value);
    }

    public function setFirstNameAttribute($value)
    {
        $this->attributes['first_name'] = encrypt($value);
    }

    public function setLastNameAttribute($value)
    {
        $this->attributes['last_name'] = encrypt($value);
    }

    public function setAddressAttribute($value)
    {
        $this->attributes['address'] = encrypt($value);
    }
    //Accesors
    public function getNameAttribute($value)
    {
        return decrypt($value);
    }

    public function getFirstNameAttribute($value)
    {
        return decrypt($value);
    }

    public function getLastNameAttribute($value)
    {
        return decrypt($value);
    }

    public function getTelephoneAttribute($value)
    {
        return decrypt($value);
    }

    public function getAddressAttribute($value)
    {
        return decrypt($value);
    }

    //realciones
    public function user()
    {
        return $this->belongsTo('App\User');
    }

}
