<?php

use Illuminate\Support\Str;
use Faker\Generator as Faker;

/**
 * Factory encargado de generar N documentos vinculado a los 
 * usuarios con rol customer.
 * @param document_number -- vector con la lista de números de documentos
 */
$factory->define(App\Models\Document::class, function (Faker $faker) {

    $document_number = ['41599459A','23436355E','27730944M','18936708A','78778134Y','60798783P',
                        '00136106S','41484427V','08779660P','10431391V','11218197Q','51410137P',
                        '30938993Z','96175207V','10326760J',];
    return [
        'file_number' => Str::random(10),
        'code' => Str::random(6),
        'title' => $faker->text($maxNbChars = 20),
        'text_document' => $faker->text($maxNbChars = 500),
        'document_number' => $faker->randomElement($document_number),
    ];
});