<?php

use Illuminate\Support\Str;
use Faker\Generator as Faker;

/**
 * Monta un listado de N mensajes para pobrar el inbox
 */
$factory->define(App\Models\InboxReceived::class, function (Faker $faker) {

    $reader = [false,true];
    $to_id = [2,3,4,5,6,7,8,9,10];
    return [
        'from_id' => $faker->randomElement($to_id),
        'to_id' =>  1,
        'from' => $faker->unique()->safeEmail,
        'to' => 'admin@admin.com',
        'parent_id' => $faker->randomElement($to_id),
        'subject' => $faker->text($maxNbChars = 20),
        'read' => $faker->randomElement($reader),
        'body' => $faker->text($maxNbChars = 500),
    ];
});