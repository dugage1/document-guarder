<?php

use Illuminate\Support\Str;
use Faker\Generator as Faker;

/**
 * Monta un listado de N mensajes para pobrar el inbox
 */
$factory->define(App\Models\InboxSent::class, function (Faker $faker) {

    $reader = [false,true];
    $to_id = [2,3,4,5,6,7,8,9,10];
    return [
        'from_id' => 1,
        'to_id' =>  $faker->randomElement($to_id),
        'from' => 'admin@admin.com',
        'to' => $faker->unique()->safeEmail,
        'subject' => $faker->text($maxNbChars = 20),
        'read' => $faker->randomElement($reader),
        'body' => $faker->text($maxNbChars = 500),
    ];
});