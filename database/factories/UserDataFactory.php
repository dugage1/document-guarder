<?php

use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\UserData::class, function (Faker $faker) {
    $document_number = ['41599459A','23436355E','27730944M','18936708A','78778134Y','60798783P',
                        '00136106S','41484427V','08779660P','10431391V','11218197Q','51410137P',
                        '30938993Z','96175207V','10326760J',];
    return [
        'name' => $faker->name,
        'first_name' => $faker->lastName,
        'last_name' => $faker->lastName,
        'way_type' => $faker->randomElement(['CL', 'PZ', 'AV']),
        'address' => $faker->streetAddress,
        'city' => $faker->randomElement(['ALCÓNTAR', 'CASTRO DE FILABRES', 'VELEFIQUE']),
        'zip' => $faker->numberBetween(41000, 41800),
        'telephone' => $faker->unique()->numberBetween(600000000, 699999999),
        'document_number' => $faker->unique()->randomElement($document_number),
        'company_name' => $faker->company,
        'province' => 'Madrid', 
        'city' => 'Madrid', 
    ];
});
