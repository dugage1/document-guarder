<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_datas', function (Blueprint $table) {
            $table->increments('id');
            //tened en cuenta, que los usuarios con rol admnin, nunca tendrán esta relación
            //ya que estos no necesitan estos datos, son reservados para rol Customer
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            //datos personales
            $table->string('name');
            $table->string('first_name');
            $table->string('last_name')->nullable();
            $table->string('company_name');
            $table->string('document_number', 10)->unique();
            //datos de contacto
            $table->string('telephone');
            $table->string('way_type', 25);
            $table->string('address');
            $table->string('province');
            $table->string('city');
            $table->string('zip', 5);
            //datos adicionales
            $table->text('contract_detail')->nullable();
            $table->text('service_specification')->nullable();

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_datas');
    }
}
