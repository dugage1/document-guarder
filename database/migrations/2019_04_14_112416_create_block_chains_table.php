<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlockChainsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('block_chains', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('index');
            $table->text('nonce')->nullable();
            $table->string('title')->nullable();
            $table->string('file_number')->nullable();
            $table->string('document_number', 10)->nullable();
            $table->string('code')->nullable();
            $table->string('timestamp');
            $table->text('data')->nullable();
            $table->text('image')->nullable();
            $table->text('signature')->nullable();
            $table->string('previous_hash');
            $table->string('hash');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('block_chains');
    }
}
