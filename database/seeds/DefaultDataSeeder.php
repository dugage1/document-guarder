<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use App\Http\Controllers\Blockchain\BlockchainController;

class DefaultDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //carga todos los tipos de vía.
        Model::unguard();

        DB::table('ways')->delete();
        $json = File::get("database/data/way.json");
        $data = json_decode($json);
        foreach ($data as $obj) {
            \App\Models\Way::create(array(
                'id' => $obj->id,
                'code' => $obj->codigo,
                'name' => $obj->nombre
            ));
        }

        Model::reguard();

        //creamos un primer bloque al desplegar la app
        Model::unguard();
        //limpiamos la tabla
        DB::table('block_chains')->delete();
        //generamos los datos del bloque
        $block = new BlockchainController();
        //guardamos el nuevo bloque
        //dump($block);
        \App\Models\BlockChainModel::create(array(
            'index' => $block->index,
            'timestamp' => $block->timestamp,
            'data' => $block->data,
            'previous_hash' => $block->previousHash,
            'hash' => $block->hash,
        ));

        Model::reguard();

        /**
         * Cargamos la colección de provincas
         */
        Model::unguard();

        DB::table('provinces')->delete();
        $json = File::get("database/data/provinces.json");
        $data = json_decode($json);
        //recorremos y cargamos la lista en la tabla provinces
        foreach ($data as $obj) {
            \App\Models\Province::create(array(
                'code' => $obj->id,
                'name' => $obj->nm
            ));
        }

        Model::reguard();

        /**
         * Cargamos la colección de municipios
         */
        Model::unguard();
        
        DB::table('cities')->delete();
        $json = File::get("database/data/cities.json");
        $data = json_decode($json);
        //recorremos y cargamos la lista en la tabla provinces
        foreach ($data as $obj) {
            \App\Models\City::create(array(
                'code' => substr($obj->id, 0,2),
                'name' => $obj->nm
            ));
        }

        Model::reguard();
    }

}