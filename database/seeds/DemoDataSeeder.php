<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class DemoDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       //Creamos 15 usuarios con rol customer con sus datos clínicos
        factory(\App\User::class, 15)->create()->each(function ($user){
            \Silber\Bouncer\BouncerFacade::assign('customer')->to($user);
            $user->user_data()->save(factory(\App\Models\UserData::class)->make());
        });
        //creamos 20 documentos en la tabla temporal de documentos, que es donde
        //se redactan los documentos antes de ser firmados
        factory(\App\Models\Document::class, 20)->create()->each(function ($document){
           
        });
        //creamos 20 mensajes para probar el inbox como  Transmisor
        factory(\App\Models\InboxSent::class, 20)->create()->each(function ($inboxSent){
           
        });
        //creamos 20 mensajes para probar el inbox como  Receptor
        factory(\App\Models\InboxReceived::class, 20)->create()->each(function ($inboxReceived){
           
        });

    }
}