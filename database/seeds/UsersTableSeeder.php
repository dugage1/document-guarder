<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = \App\User::create([
            'name' => 'Administrador',
            'email' => 'admin@admin.com',
            'password' => bcrypt('secret'),
            'remember_token' => str_random(10),
        ]);
        Bouncer::assign('admin')->to($user);
        //creamos un usuario de tipo customer con sus datos, 
        //para poder hacer test con el
        $user = \App\User::create([
            'name' => str_random(6),
            'email' => 'customer@customer.com',
            'password' => bcrypt('secret'),
            'remember_token' => str_random(10),
        ]);
        Bouncer::assign('customer')->to($user);
        //creamos los datos del usuario
        \App\Models\UserData::create([
            'user_id' => $user->id,
            'name' => 'Carlos',
            'first_name' => 'Cliente',
            'last_name' => 'Cliente',
            'way_type' => 'CL',
            'address' => 'Nenufar, 32',
            'city' => 'ALCÓNTAR',
            'zip' => 41000,
            'telephone' => 678901333,
            'document_number' => '48877890S',
            'company_name' => 'Mi Empresa SL',
            'province' => 'Madrid', 
            'city' => 'Madrid', 
        ]);

    }
}