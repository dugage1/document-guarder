/**
 * Vuejs Dialog Plugin | Para los mensajes en las acciones
 */
import VuejsDialog from "vuejs-dialog";
import VuejsDialogMixin from "vuejs-dialog/dist/vuejs-dialog-mixin.min.js";
import 'vuejs-dialog/dist/vuejs-dialog.min.css';
//pasamos la configuración lara VuejsDialog
Vue.use(VuejsDialog, {
    html: true,
    loader: false,
    okText: 'Continuar',
    cancelText: 'Cancelar',
    animation: 'bounce'
});

/**
 * Componente para la carga y busqueda en tablas
 * @param preloader -- booleano, muestra un icnono de precarga mientras se realiza la consulta
 * @param rows[] -- alamcena el resultado de la consulta
 * @param errorCode -- captura y retorna el error en la consutla si esta falla.
 * @param searchParam -- almacena los datos pasados por el campo de búsqueda de la tabla.
 */
if (document.querySelector('#table-data')) {
    //constantes con la información del módulo, base_url
    const SITE_URL = document.head.querySelector('meta[name="site-url"]').content;
    const MODULE_URL = document.head.querySelector('meta[name="module-url"]').content;
    const METHOD = document.head.querySelector('meta[name="method"]').content;

    var table_data = new Vue({

        el: '#table-data',
        data: {
            preloader: false,
            errorCode: null,
            rows: [],
            searchParam:'',
            timeout: null,
        },
        methods: {

            /**
             * Método que es llamado desde el campo de búsqueda pasando o no datos que son almacenados
             * en @param searchParam
             */
            getDataBySearchParam : function() {

                if (this.timeout != null) {
                    clearTimeout(this.timeout);
                }

                this.timeout = setTimeout( () => {

                    this.timeout = null;
                    let method = METHOD != 'index' ? '/' + METHOD + '/' : '';
                    let url = this.searchParam ? SITE_URL+'/'+MODULE_URL+'/search/'+ method.substring(1) + this.searchParam : SITE_URL+'/'+MODULE_URL + method;
                    //alert(url);
                    //si el parametro es vacío loadData sin param
                    url != '' ?  this._loadData(url) : this._loadData();

                }, 500);

            },
            /**
             * Método encargado de eliminar de forma definitiva el datos mediante su ID
             * Este método solicita medinate VuejsDialog la confirmación nates de realizar la acción
             * en caso positivo, llama de forma interna al metodo _destroy, que es quien realiza
             * la accion de destroy realmente.
             * @param id -- id del datos a borrar
             */
            detroyData: function(text,id){
                //solicitamos al usuario que confirme la acción
                this.$dialog.confirm(text)
                .then((dialog) => {

                    this._destroy(id);

                })
                .catch(function () {

                    console.log('Clicked on cancel');

                });
                
            },
            /**
             * Elimina de forma definitiva el dato mediante su id
             * @param url -- almacena la url para la consulta
             * @param id -- id del datos a borrar
             */
            _destroy(id){
                //montamos la url para la consulta
                let url = SITE_URL + '/'+MODULE_URL + '/destroy/' + id;
                METHOD != 'index' ? url = SITE_URL+'/' + MODULE_URL + '/' + METHOD + '/destroy/'+id : '';
                //mediante ajax realizamos la consulta para destruir el dato
                axios.delete(url).then((response) => {
                   
                    //llamamos a loadData, para recargar el contenido
                    this._loadData();

                }).catch(error => {
                    this.errorCode = error.response;
                });
            },
            /**
             * Este método privado, es encargado de lanzar las consultas y retornar una colección de datos
             * @param urlPage -- dato que puede ser nulo, en caso de ser necesario, podemos
             * @param url -- parametro interno donde cargamos la url para la consutla de datos
             * pasar la url de la consulta
             */
            _loadData(urlPage = null) {
                //mostramos el preload
                this.preloader = true;
                //montamos la url para la consutla, el parametro es cargado al inicio 
                //del método apuntanto a lo que sería nuestro index.
                let url = SITE_URL+'/'+MODULE_URL;
                //si METHOD es distinto de  index añadimos este a la url
                METHOD != 'index' ? url += '/' + METHOD : '';
                //si urlPage es distinto de null, entonces sobreescribimos url
                urlPage != null ? url = urlPage : '';
                //realizamos la consulta mediante ajax
                axios.get(url).then((response) => {
                    //pasamos los datos de la consutla
                    this.rows = response.data;
                    //ocultamos el preload
                    this.preloader = false;

                }).catch(error => {

                    this.errorCode = error.response;
                    this.preloader = false;
                });

            }
        },
        mounted() {
            this._loadData();
        },


    });
}