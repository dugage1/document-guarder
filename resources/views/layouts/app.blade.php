<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

    @section('htmlheader')
        @include('layouts.partials.htmlheader')
    @show

    <body class="kt-page--fixed kt-page-content-white kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent">

        <div class="kt-grid kt-grid--hor kt-grid--root">

			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">

                @if( Request::segment(1) == 'login')

                    @include('layouts.auth.login')

                @else

                    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper " id="kt_wrapper">

                    @include('layouts.partials.mainheader')

                    @include('layouts.partials.menutab')

                    @include('layouts.partials.content')

                    </div>

                @endif

            </div>

        </div>

        @include('layouts.partials.footer')

        @section('scripts')
            @include('layouts.partials.scripts')
        @show

    </body>

</html>
