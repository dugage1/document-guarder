<div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v4 kt-login--signin" id="kt_login">

    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" style="background-image: url({{asset('/images/bg-2.jpg')}});">

        <div class="kt-grid__item kt-grid__item--fluid kt-login__wrapper">

            <div class="kt-login__container">

                <div class="kt-login__logo">

                    <a href="#"><img src="{{asset('/images/logo-5.png')}}"></a>

                </div>

                <div class="kt-login__signin">

                    <div class="kt-login__head">

						<h3 class="kt-login__title">{{ trans('app.signin') }}</h3>

                        <form class="kt-form" method="POST" action="{{ route('login') }}">
                            @csrf

                            <div class="input-group">

                                <input id="email" type="email" placeholder="{{ __('E-Mail Address') }}" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif

                            </div>

                            <div class="input-group">

                                <input id="password" placeholder="{{ __('Password') }}" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            
                            </div>

                            <div class="row kt-login__extra">

                                <div class="col">
                                    <label class="kt-checkbox">
                                        <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Remember Me') }}
                                        <span></span>
                                    </label>
                                </div>

                                @if (Route::has('password.request'))
                                    <div class="col kt-align-right">
                                        <a id="kt_login_forgot" class="kt-login__link" href="{{ route('password.request') }}">
                                            {{ __('Forgot Your Password?') }}
                                        </a>
                                    </div>
                                @endif

                            </div>

                            <div class="kt-login__actions">
                                <button  type="submit" class="btn btn-brand btn-pill kt-login__btn-primary">{{ __('Login') }}</button>
                            </div>

                        </form>

					</div>

                </div>

            </div>

        </div>

    </div>

</div>