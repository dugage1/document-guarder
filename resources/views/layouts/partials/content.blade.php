<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-grid--stretch">

    <div class="kt-container kt-body  kt-grid kt-grid--ver" id="kt_body">
        
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">

            <div class="kt-subheader kt-grid__item" id="kt_subheader">

                <div class="kt-subheader__main">

                    <h3 class="kt-subheader__title">@yield('subheader_title')</h3>

                    <span class="kt-subheader__separator kt-subheader__separator--v"></span>

                    <span class="kt-subheader__desc">@yield('subheader_desc')</span>
                    
                    <!--<a href="#" class="btn btn-label-primary btn-bold btn-icon-h kt-margin-l-10">
                        {{ trans('app.add') }}
                    </a>-->

                </div>

            </div>

            <div class="kt-content kt-grid__item kt-grid__item--fluid">

                <div class="row">
                    @yield('content')
                </div>

            </div>

        </div>

    </div>

</div>