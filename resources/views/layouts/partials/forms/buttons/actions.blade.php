<div class="kt-form__actions">

    <template v-if="isLoading">

        @if( Request::segment(1) == 'inbox' )
            <button type="button" class="btn btn-primary disabled"><i class="flaticon-multimedia-2"></i> {{ trans('app.inbox.sending') }}</button>
        @else
            <button type="button" class="btn btn-primary disabled"><i class="flaticon-interface-4"></i> {{ trans('app.loadingData') }}</button>
        @endif
            
    </template>

    <template v-else-if="!isLoading && !saved">

        @if( Request::segment(1) == 'inbox' )
            <button @click="saveData" type="button" class="btn btn-primary"><i class="flaticon-multimedia-2"></i> {{ trans('app.inbox.send') }}</button>
        @else
            <button @click="saveData" type="button" class="btn btn-primary"><i class="flaticon-file-1"></i> {{ trans('app.save') }}</button>
        @endif
        
    </template>

    <template v-else-if="saved">

        <div class="alert alert-success" role="alert">{{ trans('app.theActionYouHaveTakenHasBeenSuccessful') }}</div>
        <a :href="routes.index" class="btn btn-success">{{ trans('app.back') }} {{ trans('app.toList') }}</a>
        <a :href="routes.create" class="btn btn-primary">{{ trans('app.create') }} {{ trans('app.new') }}</a>
        @if( Request::segment(1) != 'inbox' && Request::segment(2) != 'edit' )
            <a :href="routes.edit" class="btn btn-info">{{ trans('app.edit') }}</a>
            <a :href="routes.index" class="btn btn-success">{{ trans('app.back') }} {{ trans('app.toList') }}</a>
            <a :href="routes.create" class="btn btn-primary"><i class="flaticon-multimedia-2"></i> {{ trans('app.create') }} {{ trans('app.new') }}</a>
        @endif

    </template>

</div>

<template v-if="errorCode">

    <div v-for="error in errorCode.data.errors" class="kt-form__actions">
        <span class="alert-danger" v-text="error[0]"></span><br/>
    </div>

</template>