<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- URL SITE -->
    <meta name="site-url" content="{{ config('app.url') }}">
    @yield('meta')
    <title>{{ config('app.name', 'Panel de administración') }}</title>

    <!-- Styles -->
    @if( Request::segment(1) == 'login')
        <link href="{{ asset('css/login-v4.demo10.min.css') }}" rel="stylesheet">
    @endif
    <link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet"> 
    <link href="{{asset('css/flaticon/flaticon.css')}}" rel="stylesheet">
    <link href="{{asset('css/style.bundle.min.css')}}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

</head>