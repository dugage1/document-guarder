<div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed " data-ktheader-minimize="on">

    <div class="kt-header__top">

        <div class="kt-container">

            <!-- begin:: Brand -->
			<div class="kt-header__brand kt-grid__item" id="kt_header_brand">
                
                <div class="kt-header__brand-logo">

                <a href="#">

                    <img width="164px"; alt="Logo" src="{{asset('/images/logo-10.png')}}" class="kt-header__brand-logo-default">
                
                </a>

            </div>

        </div>

        <div class="kt-header__topbar kt-grid__item kt-grid__item--fluid">
            
            <div class="kt-header__topbar-item" id="kt_quick_search_toggle">
                
                <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px">

                @if(\App\Helpers\Inbox::getNumMessagesNotRead() == 0)
                    <span class="kt-header__topbar-icon kt-header__topbar-icon--success"><i class="flaticon-alert"></i></span>
                @else
                    <span class="kt-header__topbar-icon kt-header__topbar-icon--danger"><i class="flaticon-alert"></i></span>
                @endif
                </div>

            </div>

            <div class="kt-header__topbar-item" id="kt_quick_search_toggle">
            
                <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px">
                    <span onclick="event.preventDefault();document.getElementById('logout-form').submit();" class="kt-header__topbar-icon kt-header__topbar-icon--primary"><i class="flaticon-logout"></i></span>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
                </div>

            </div>

        </div>

    </div>

</div>