<div class="kt-header__bottom">

    <div class="kt-container">

        <!-- begin: Header Menu -->
        <button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
        
        <div class="kt-header-menu-wrapper" id="kt_header_menu_wrapper">
            
            <div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile ">

                <ul class="kt-menu__nav ">

                @if (Auth::User()->isA('admin'))
                    
                    <li class="kt-menu__item {{ Request::segment(1) == '' || Request::segment(1) == 'home' ? 'kt-menu__item--active' : '' }}" aria-haspopup="true"><a href="{{ route('home') }}" class="kt-menu__link "><span class="kt-menu__link-text">{{ trans('app.dashBoard') }}</span></a></li>
                    <li class="kt-menu__item {{ Request::segment(1) == 'customer' ? 'kt-menu__item--active' : '' }}" aria-haspopup="true"><a href="{{ route('customer') }}" class="kt-menu__link "><span class="kt-menu__link-text">{{ trans('app.customers.customers') }}</span></a></li>
                
                @endif

                @if (Auth::User()->isA('customer'))

                    <li class="kt-menu__item {{ Request::segment(1) == 'myprofile' ? 'kt-menu__item--active' : '' }}" aria-haspopup="true"><a href="{{ route('myprofile') }}" class="kt-menu__link "><span class="kt-menu__link-text">{{ trans('app.customers.myProfile') }}</span></a></li>
                
                @endif

                    <li class="kt-menu__item {{ Request::segment(1) == 'document' || Request::segment(1) == 'contract'  ? 'kt-menu__item--active' : '' }}" aria-haspopup="true"><a href="{{ route('document') }}" class="kt-menu__link "><span class="kt-menu__link-text">{{ trans('app.documents.documents') }}</span></a></li>
                    <li class="kt-menu__item {{ Request::segment(1) == 'inbox' ? 'kt-menu__item--active' : '' }}" aria-haspopup="true"><a href="{{ route('inbox') }}" class="kt-menu__link "><span class="kt-menu__link-text">{{ trans('app.alerts.alerts') }}</span>
                    @if(\App\Helpers\Inbox::getNumMessagesNotRead() > 0)
                        <span class="kt-menu__link-badge"><span class="kt-badge kt-badge--danger">{{\App\Helpers\Inbox::getNumMessagesNotRead()}}</span></span>
                    @endif
                    </a></li>
                    
                </ul>

            </div>

        </div>
        <!-- end: Header Menu -->

    </div>

</div>