<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
@yield('js')
<!-- Fonts -->
<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
<script>
    WebFont.load({
        google: {
            "families": ["Poppins:300,400,500,600,700", "Asap+Condensed:500"]
        },
        active: function() {
            sessionStorage.fonts = true;
        }
    });
</script>