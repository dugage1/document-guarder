@extends('mails.layouts.master')

@section('content')
    <h3>Hola, {{$userData->name}}</h3>
    <p class="lead">{{ trans('app.documents.sendDocumentMessage') }}</p>
    <h5 class="">{{ trans('app.code') }}: {{$code}}</h5>
    <p>{{ trans('app.documents.sendDocumentMessageBis') }}</p>
    <!-- Callout Panel -->
    <p class="callout">
    {{ trans('app.documents.sendDocumentMessageGoToDocument') }} <a href="{{ route('document.show',$document->id) }}">Click! &raquo;</a>
    </p><!-- /Callout Panel -->	
@stop