const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css')
   .js('resources/js/tableData.js', 'js/table_data.js')
   .js('Modules/Customer/Resources/assets/js/createOrUpdate.js', 'js/customer_create_or_update.js')
   .js('Modules/Document/Resources/assets/js/createOrUpdate.js', 'js/document_create_or_update.js')
   .js('Modules/Document/Resources/assets/js/permissionShowDocument.js', 'js/permission_show_document.js')
   .js('Modules/Document/Resources/assets/js/signatureDocument.js', 'js/signature_document.js')
   .js('Modules/Contract/Resources/assets/js/showContract.js', 'js/show_contract.js')
   .js('Modules/Myprofile/Resources/assets/js/myProfile.js', 'js/my_profile.js')
   .js('Modules/Inbox/Resources/assets/js/createOrShow.js', 'js/create_or_show.js')
   .js('Modules/Inbox/Resources/assets/js/buttonsBar.js', 'js/buttons_bar.js');
